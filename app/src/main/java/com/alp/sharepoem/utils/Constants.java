package com.alp.sharepoem.utils;

import android.graphics.Bitmap;

public class Constants {
    public static String ANONIMYA_URL="http://192.168.43.47:8080";
    public static String URL=ANONIMYA_URL + "/api/";
    public static String PUBLIC_URL="";
    public static String UPLOAD_HOST_PROD="";
    public final static String DEFAULT="DEFAULT";

    public final static String URL_USERS_LOGIN ="users/login";
    public final static String URL_USERS_SEND_PASSWORD="users/send-password";
    public final static String URL_SIGN_UP="users/signup";
    public final static String URL_GET_USERS="users/list";
    public final static String URL_GET_USER ="users/";

    public final static String URL_GET_POSTS="posts/list";
    public final static String URL_GET_POSTS_FOR_CATEGORIES="posts/getPostsForCategory";
    public final static String URL_ADD_POST="posts/addPost";
    public final static String URL_ADD_PART="posts/addPart";
    public final static String URL_GET_POST_PART ="posts/getPostPart";
    public final static String URL_GET_POST_PART_CHILD ="posts/getPostPartChild";
    public final static String URL_GET_PARTS_OF_POST ="posts/getPartsOfPost";
    public final static String URL_GET_SEQUENCE ="posts/getSequence";
    public final static String URL_GET_POSTS_OF_PROFILE="posts/getUserPostsForProfile";

    public final static String URL_ADD_FOLLOWERS="follows/addFollowers";
    public final static String URL_GET_MY_FOLLOWERS="follows/getMyFollowers";
    public final static String URL_GET_MY_FOLLOWEDS="follows/getMyFolloweds";
    public final static String URL_REMOVE_MY_FOLLOWEDS="follows/remove";

    public final static String URL_GET_MY_NOTIFICATIONS="natifications/getMyNatifications";
    public final static String URL_GET_CATEGORIES="categories/list";

    public final static String[] TABS = {"main", "search", "notification", "profile"};

}
