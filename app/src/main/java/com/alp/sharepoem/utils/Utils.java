package com.alp.sharepoem.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Utils {


    public static float getUnitSize(Context context) {
        return getTabHeight(context) * 0.77f;
    }

    public static float getActionHeight(Context context) {

        return getTabHeight(context) * 0.77f;
    }

    public static final int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getTextHeight(TextView textView, int width) {
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public static int getTextWidth(TextView textView, int width) {
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredWidth();
    }

    public static float getTabHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (float) (dm.widthPixels * 0.1573333333f * 1.14);
    }

    public static int getLeftMargin(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        return vp.leftMargin;
    }

    public static void setViewLeftMargin(View view, int leftMargin) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        vp.leftMargin = leftMargin;
        view.setLayoutParams(vp);
    }

    public static int getRightMargin(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        return vp.rightMargin;
    }

    public static void setViewRightMargin(View view, int rightMargin) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        vp.rightMargin = rightMargin;
        view.setLayoutParams(vp);
    }

    public static int getTopMargin(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        return vp.topMargin;
    }

    public static void setViewTopMargin(View view, int topMargin) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        vp.topMargin = topMargin;
        view.setLayoutParams(vp);
    }

    public static int getBottomMargin(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        return vp.bottomMargin;
    }

    public static void setViewBottomMargin(View view, int bottomMargin) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        vp.bottomMargin = bottomMargin;
        view.setLayoutParams(vp);
    }

    public static int getRight(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        int i = vp.leftMargin + vp.width;
        return i;
    }

    public static int getBottom(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        int i = vp.topMargin + vp.height;
        return i;
    }

    public static int getTop(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        int i = vp.bottomMargin + vp.height;
        return i;
    }

    public static int getLeft(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        int i = vp.rightMargin + vp.width;
        return i;
    }

    public static void setViewHeight(View view, int height) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        vp.height = height;
        view.setLayoutParams(vp);
    }

    public static int getViewHeight(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        return vp.height;
    }

    public static void setViewWidth(View view, int width) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        vp.width = width;
        view.setLayoutParams(vp);
    }

    public static int getViewWidth(View view) {
        FrameLayout.LayoutParams vp = (FrameLayout.LayoutParams) view.getLayoutParams();
        return vp.width;
    }

    public static float getEditTextSize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (float) (dm.widthPixels * 80 / 478) * 0.65f * 0.5f;
    }


    public static float getElementSize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.widthPixels * 80 / 478);
    }

    public static float getButtonTextSize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return ((dm.widthPixels * 96 / 289) * 0.65f) * 0.2f;
    }

    public static int getWidthPixels(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.widthPixels;
    }

    public static GradientDrawable setCircleCorner() {
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        // gd.setColor(Color.rgb(134, 135, 255));
        gd.setCornerRadius(15);
        gd.setStroke(4, Color.rgb(36, 189, 197));
        return gd;
    }

    public static int getHeightPixels(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.heightPixels;
    }


    public static String getStringFromRes(Activity activity, int resId) {
        String str = "";
        str = activity.getString(resId);
        return str;
    }
    public static boolean isEmpty(CharSequence str) {
        return str == null || isEmpty(str.toString());
    }

    public static boolean isEmpty(String str) {
        return str == null || str.equals("") || str.equals("null");
    }

    public static String getTime() {
        Calendar c = Calendar.getInstance();
        String hour = "", minute = "";
        if (String.valueOf(c.get(Calendar.HOUR_OF_DAY)).length() == 1)
            hour = "0" + String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        else
            hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));

        if (String.valueOf(c.get(Calendar.MINUTE)).length() == 1)
            minute = "0" + String.valueOf(c.get(Calendar.MINUTE));
        else
            minute = String.valueOf(c.get(Calendar.MINUTE));

        return hour + ":" + minute;
    }

    public static String getTimeText(Calendar c) {

        String hour = "", minute = "";
        if (String.valueOf(c.get(Calendar.HOUR_OF_DAY)).length() == 1)
            hour = "0" + String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        else
            hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));

        if (String.valueOf(c.get(Calendar.MINUTE)).length() == 1)
            minute = "0" + String.valueOf(c.get(Calendar.MINUTE));
        else
            minute = String.valueOf(c.get(Calendar.MINUTE));

        return hour + ":" + minute;
    }


    public static String getStringDate() {
        Calendar gc = Calendar.getInstance();
        String finishDate = ((String.valueOf(gc.get(Calendar.DAY_OF_MONTH)).length() == 1) ? "0" : "") + String.valueOf(gc.get(Calendar.DAY_OF_MONTH)) + "/" +
                ((String.valueOf(gc.get(Calendar.MONTH) + 1).length() == 1) ? "0" : "") + String.valueOf(gc.get(Calendar.MONTH) + 1) + "/" +
                String.valueOf(gc.get(Calendar.YEAR));

        return finishDate;
    }



    public static int getDayOfWeek(Calendar calendar) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 2;
        if (dayOfWeek == -1)
            return 6;
        else
            return dayOfWeek;
    }

    public static boolean checkPhone(String phone) {
        if (phone.length() >= 10)
            return true;
        else
            return false;
    }

    public static boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }

        return hasImage;
    }

    public static String getToday(int cursorDay) {
        if (cursorDay == 0)
            return "Pazartesi";
        else if (cursorDay == 1)
            return "Salı";
        else if (cursorDay == 2)
            return "Çarşamba";
        else if (cursorDay == 3)
            return "Perşembe";
        else if (cursorDay == 4)
            return "Cuma";
        else if (cursorDay == 5)
            return "Cumartesi";
        else
            return "Pazar";
    }

    public static void hideKeyboard(Context context) {

        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

            View view = ((Activity) context).getCurrentFocus();
            if (view != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showKeyboard(Context context) {

        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

            View view = ((Activity) context).getCurrentFocus();
            if (view != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_IMPLICIT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showDialogMessage(final Context activity, final int type, final String title, final String msg) {
        ((Activity) activity).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(activity).create();
                dialog.setTitle(title);
                dialog.setMessage(msg);

                if(type == 0)
                    dialog.setCanceledOnTouchOutside(true);
                else
                    dialog.setCanceledOnTouchOutside(false);

                dialog.setButton(DialogInterface.BUTTON_NEUTRAL, getStringFromRes((Activity) activity, R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                dialog.show();
            }
        });
    }

    public static String getIsonDateFormat(Date date){
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        return inFormat.format(date);
    }

    public static Typeface getRegularFont(Activity activity){
        return Typeface.createFromAsset(activity.getAssets(), "fonts/Nunito-Regular.ttf");
    }

    public static Typeface getBoldFont(Activity activity){
        return Typeface.createFromAsset(activity.getAssets(), "fonts/Nunito-Bold.ttf");
    }

    public static void showDialogMessageAndGoBack(final Context activity, final String title, final String msg) {
        ((Activity) activity).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(activity).create();
                dialog.setTitle(title);
                dialog.setMessage(msg);
                dialog.setCanceledOnTouchOutside(false);

                //dialog.setCanceledOnTouchOutside(true);
                dialog.setButton(DialogInterface.BUTTON_NEUTRAL, getStringFromRes((Activity) activity, R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ((Activity) activity).onBackPressed();
                            }
                        });
                dialog.show();
            }
        });
    }

    public static int getNavigationBarHeight(Context context){
        boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
        int resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0 && !hasMenuKey)
        {
            return context.getResources().getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static boolean hasNavBar(){

        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);

        return (!(hasBackKey && hasHomeKey));
    }

/*
    public static void setImageFromWeb(String imgUrl, ImageView imgView, Context context){
        ImageLoader.getInstance().loadImage(imgUrl, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Constants.setAlbumImageBitmap(loadedImage, true);
                imgView.setImageBitmap(loadedImage);
            }
        });
    }*/

  /*  public static void updateImage(final MediaDescriptionCompat description, final boolean isPlayable,
                                   final ImageView imageView, final ImageFetcher imageFetcher) {
        if (description.getIconBitmap() != null) {
            imageView.setImageBitmap(description.getIconBitmap());
        } else {
            final Uri iconUri = UrlBuilder.preProcessIconUri(description.getIconUri());
            if (iconUri != null) {
                //FabricUtils.log(MediaItemsAdapter.class.getSimpleName() + " icon:" + iconUri.toString());
            }
            if (isPlayable) {
                if (iconUri != null && iconUri.toString().startsWith("android")) {
                    imageView.setImageURI(iconUri);
                } else {
                    // Load the image asynchronously into the ImageView, this also takes care of
                    // setting a placeholder image while the background thread runs
                    imageFetcher.loadImage(iconUri, imageView);
                }
            } else {
                imageView.setImageURI(iconUri);
            }
        }
    }
*/
    public static void updateImage(final Bitmap bitmap, final ImageView imageView) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
    }
/**
    public static void updateImage(Uri iconUri,
                                   final ImageView imageView, final ImageFetcher imageFetcher) {
        if(iconUri == null)
            return;
        iconUri = UrlBuilder.preProcessIconUri(iconUri);
        if (iconUri != null) {
            //FabricUtils.log(MediaItemsAdapter.class.getSimpleName() + " icon:" + iconUri.toString());
            //return;
        }

        if (iconUri != null && iconUri.toString().startsWith("android")) {
            imageView.setImageURI(iconUri);
        } else {
            // Load the image asynchronously into the ImageView, this also takes care of
            // setting a placeholder image while the background thread runs
            imageFetcher.loadImage(iconUri, imageView);
        }
    }

    public static void updatePlayBarImage(Uri iconUri,
                                          final ImageView imageView,
                                          final ImageFetcher imageFetcher,
                                          Context context) {
        if(iconUri == null)
            return;
        iconUri = UrlBuilder.preProcessIconUri(iconUri);
        if (iconUri != null) {
            //FabricUtils.log(MediaItemsAdapter.class.getSimpleName() + " icon:" + iconUri.toString());
            //return;
        }

        if (iconUri != null && iconUri.toString().startsWith("android")) {
            imageView.setImageURI(iconUri);
        } else {
            // Load the image asynchronously into the ImageView, this also takes care of
            // setting a placeholder image while the background thread runs
            imageFetcher.loadPlayBarImage(iconUri, imageView, context);
        }
    }

    public static void updateImage(String url,
                                   final ImageView imageView, final ImageFetcher imageFetcher) {

        if(isEmpty(url))
            return;
        Uri iconUri = UrlBuilder.preProcessIconUri(Uri.parse(url));
        if (iconUri != null) {
            //FabricUtils.log(MediaItemsAdapter.class.getSimpleName() + " icon:" + iconUri.toString());
            //return;
        }

        if (iconUri != null && iconUri.toString().startsWith("android")) {
            imageView.setImageURI(iconUri);
        } else {
            // Load the image asynchronously into the ImageView, this also takes care of
            // setting a placeholder image while the background thread runs
            imageFetcher.loadImage(iconUri, imageView);
        }

    }*/

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        reqWidth = width/2;
        reqHeight = height/2;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
/*
    public static void setAlbumImageBySubTitle(String subTitle, ImageView imgView, Context context){
        String[] s = subTitle.split(" - ");
        String imgUrl = "https://itunes.apple.com/search?term=%"+s[0]+"+%"+s[1]+"&entity=song";
        setImageFromWeb(subTitle, imgView, context);
    }
*/
    public static String decrypt(final String hash) {
        try {
            return new String(xor(android.util.Base64.decode(hash.getBytes(), Base64.DEFAULT)), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private static byte[] xor(final byte[] input) {
        String password = "array-index-out-of-bound-exception";
        final byte[] output = new byte[input.length];
        final byte[] secret = (password).getBytes();

        int spos = 0;
        for (int pos = 0; pos < input.length; ++pos) {
            output[pos] = (byte) (input[pos] ^ secret[spos]);
            spos += 1;
            if (spos >= secret.length) {
                spos = 0;
            }
        }
        return output;
    }

    private static StringBuilder sSearchQuery = new StringBuilder();

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static boolean hasVersionM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    /**
     * Save Search query string.
     *
     * @param searchQuery Search query string.
     */
    public static void setSearchQuery(final String searchQuery) {
        clearSearchQuery();
        sSearchQuery.append(searchQuery);
    }

    /**
     * @return Gets the Search query string.
     */
    public static String getSearchQuery() {
        return sSearchQuery.toString();
    }

    /**
     * Clear Search query string.
     */
    public static void clearSearchQuery() {
        sSearchQuery.setLength(0);
    }

    public static int getPlayBarHeight(Context context){
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return  dm.widthPixels * 168 / 1080;
    }

    public static ImageView getLine(Context context){
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.getHeightPixels(context) * 3 / 1334);
       // lp.gravity = Gravity.CENTER;
        ImageView line = new ImageView(context);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        return line;

    }

    public static void statusBarLightIconColorChange(Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN| View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            activity.getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    public static void statusBarDarkIconColorChange(Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            // edited here
            activity.getWindow().setStatusBarColor(Color.BLACK);
        }
    }

    public static String firstLetterToUpper(String input){
        String[] words = input.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            sb.append(" ");
            if (!words[i].equals(""))
                sb.append(Character.toUpperCase(words[i].charAt(0)) + words[i].subSequence(1, words[i].length()).toString().toLowerCase());
            else
                sb.append("");
        }
        return sb.toString();
    }
}
