package com.alp.sharepoem.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.alp.sharepoem.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;


public class Session {
    SharedPreferences pref;

    Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "user";

    private static final String IS_LOGIN = "IsLoggedIn";

    private static Session instance = null;

    public static Session getSession(Context context) {
        if (instance == null)
            instance = new Session(context);
        return instance;
    }

    public Session(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(JSONObject response) {
        signOut();
       /* try {
            editor.putString("token",response.getString("token"));
            editor.putString("profile",response.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        editor.commit();
    }

    public void getUserInformation(String key) {

        try {
            JSONObject jo = new JSONObject(getString("profile"));
            JSONObject jUser = (JSONObject) jo.get("user");
            JSONObject jUserInfo = (JSONObject) jUser.get("userInformation");
            jUserInfo.getString(key);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void putBoolean(String key, Boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void putInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return pref.getBoolean(key, false);
    }

    public int getInt(String key) {
        return pref.getInt(key,0);
    }

    public void remove(String key) {
        editor.remove(key);
        editor.commit();
    }

    public String getString(String key) {
        return pref.getString(key, "");
    }

    public boolean has(String key) {
        return !Utils.isEmpty(getString(key));
    }


    public void signOut() {
        editor.clear();
        editor.commit();
    }

    public boolean isLoggedIn() {

        return pref.getBoolean(IS_LOGIN, false);
    }


}
