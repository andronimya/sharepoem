package com.alp.sharepoem.fragment.root;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.fragment.SearchFragment;

import java.util.ArrayList;
import java.util.List;

public class SearchRootFragment extends TabFragment {
    List<Fragment> stackFragment = new ArrayList<>();
    FrameLayout container;

    boolean created = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        if(!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = (FrameLayout) inflater.inflate(R.layout.fragment_search, cont,
                false);
        if(container.getParent()!=null)
            ((FrameLayout)container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void addFragment(Fragment fragment){
        stackFragment.add(fragment);
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.search_root_frame, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (created)
            return;
        SearchFragment searchFragment = new SearchFragment(getActivity());
        addFragment(searchFragment);
        created = true;
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void clearStackFragment(){
        created = false;
        Fragment fragment = null;
        if(stackFragment != null || stackFragment.size()>1)
            fragment = stackFragment.get(0);
        if(fragment != null){
            stackFragment.clear();
            addFragment(fragment);
        }
    }


    public void popFragment(){
        if(stackFragment.size() == 1){
            ((MainActivity)getActivity()).close();
            return;
        }

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.search_root_frame, stackFragment.get(stackFragment.size()-2));
        fragmentTransaction.commit();
        stackFragment.remove(stackFragment.size()-1);
    }

    public void onCountryChange(){
        created = false;
        stackFragment.clear();
    }
}
