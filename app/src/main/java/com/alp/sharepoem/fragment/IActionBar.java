package com.alp.sharepoem.fragment;


import com.alp.sharepoem.view.ActionBar;

public interface IActionBar {
    void renderActionBar(ActionBar actionBar);
}
