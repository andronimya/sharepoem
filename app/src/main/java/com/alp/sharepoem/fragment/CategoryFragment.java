package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;
import com.alp.sharepoem.view.EmptyView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class CategoryFragment extends Fragment implements Remotable {
    FrameLayout container, scrollFrame, languageFrame, recordFrame, countryFrame, myRecordsFrame, localFrame, popularFrame, lastPlayedFrame, sleepModeFrame;
    int width, height, sb, actionHeight, elementSize, tabHeight, logoSize, frameHeight;
    MainActivity activity;
    Session session;
    boolean created = false;
    Typeface mediumfont;
    CategoryAdapter adapter;
    ListView listView;
    JSONObject user;
    EmptyView emptyView;

    public CategoryFragment(Context context) {
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        actionHeight = (int) Utils.getActionHeight(activity);
        tabHeight = (int) Utils.getTabHeight(activity);
        elementSize = (height - actionHeight - tabHeight) / 6;
        logoSize = width * 140 / 1080;
        session = Session.getSession(activity);
        try {
            user = new JSONObject(session.getString("user"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.getActionBr().render();
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);

        /*if (created) {
            Utils.setViewHeight(container, frameHeight);
            scrollFrame.setMinimumHeight((frameHeight>(elementSize * 8))?frameHeight:elementSize * 8);
            return;
        }*/

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin = actionHeight;
        container.setLayoutParams(cp);
        render();

        getCategroies();
    }

    public void render() {

        FrameLayout.LayoutParams lvp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        //lvp.topMargin = (int) (height*15/1334+u);
        listView = new ListView(activity);
        listView.setLayoutParams(lvp);
        listView.setDividerHeight(0);
        adapter = new CategoryAdapter(activity);
        listView.setAdapter(adapter);
        listView.setDividerHeight(2);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    HomePageFragment homePageFragment = new HomePageFragment(activity,adapter.getItem(position).getString("id"));
                    activity.addFragment(homePageFragment);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        container.addView(listView);
    }



    public class CategoryAdapter extends ArrayAdapter<JSONObject> {

        private class ViewHolder {
            FrameLayout mainFrame;
            TextView contentText;
            JSONObject jData;
            FrameLayout.LayoutParams mfp, ctp;

            public ViewHolder(int position) {
                mfp = new FrameLayout.LayoutParams(width , elementSize);
                mfp.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
                mainFrame = new FrameLayout(getContext());
                mainFrame.setLayoutParams(mfp);
                mainFrame.setBackgroundColor(Color.WHITE);
                //mainFrame.setBackgroundResource(R.drawable.shadow_2749);

                ctp = new FrameLayout.LayoutParams(width - width * 40 / 1080 , elementSize/2);
                ctp.gravity = Gravity.CENTER_VERTICAL;
                ctp.leftMargin = width * 20 / 1080;
                contentText = new TextView(getContext());
                contentText.setLayoutParams(ctp);
                contentText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(getContext()) * 5 / 8) * 0.50f);
                contentText.setGravity(Gravity.CENTER_VERTICAL);
                contentText.setTextColor(Color.BLACK);

                mainFrame.addView(contentText);

            }

            public void setData(JSONObject jData) {
                this.jData = jData;
                try {
                    contentText.setText(jData.getString("genre"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        public CategoryAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = new FrameLayout(getContext());

                holder = new ViewHolder(position);

                ((FrameLayout) convertView).addView(holder.mainFrame);
                /*AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize+height*40/1334);
                convertView.setLayoutParams(cp);*/
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            holder.setData(getItem(position));
            AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize);
            //cp.height = measureHeight + height*40/1334;
            convertView.setLayoutParams(cp);
            return convertView;
        }
    }





    @Override
    public void responsed(String method, final JSONObject response) {
        if (method.equals("getCategroies")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adapter.clear();
                        JSONArray categories = response.getJSONArray("array");
                        for (int i = 0; i < categories.length(); i++) {
                            adapter.add(categories.getJSONObject(i));
                        }
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        }
    }

    @Override
    public void failed(String method, JSONObject response) {

    }

    public void getCategroies() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();


        remote.callAsync("GET", Constants.URL + Constants.URL_GET_CATEGORIES , params, "getCategroies", true);
    }
}

