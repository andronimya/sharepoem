package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;
import com.alp.sharepoem.view.EmptyView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


@SuppressLint("ValidFragment")
public class MessageFragment extends Fragment implements IActionBar,Remotable {
    FrameLayout container, lastSearchedFrame;
    int width, height, sb, actionHeight, tabHeight, elementSize, logoWidth;
    MainActivity activity;
    Session session;
    boolean created = false;
    ListView listView;
    TextView titleText, lastSearchTitle;
    ImageView searchIcon;
    Typeface mediumfont;
    EditText searchEt;
    FrameLayout.LayoutParams lvp;
    UsersAdapter adapter;
    //EmptyView emptyView;
    ArrayList<JSONObject> elements;
    boolean elementsStatus = true;

    public MessageFragment(Context context) {
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        actionHeight = (int) Utils.getActionHeight(activity);
        tabHeight = (int) Utils.getTabHeight(activity);
        elementSize = height * 200 / 1920;
        session = Session.getSession(activity);
        elements = new ArrayList<>();
        logoWidth = width * 170 / 1080;
        /*emptyView = new EmptyView(activity, "search_empty_view",
                "Kullanıcıları Ara", "Uygulamadaki kullanıcıları burada arayabilirsiniz.");*/
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created){
            container = new FrameLayout(getActivity());
            //container.addView(emptyView);
        }

        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin = actionHeight;
        container.setLayoutParams(cp);
        //container.setBackgroundColor(Color.BLUE);

        render();
        created = true;
    }

    public void render() {
        lvp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        listView = new ListView(activity);
        listView.setLayoutParams(lvp);
        listView.setDividerHeight(0);
        listView.setFocusable(false);
        adapter = new UsersAdapter(activity);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        container.addView(listView);


        getUsers();
    }


    public class UsersAdapter extends ArrayAdapter<JSONObject> {
        Filter filter;

        @Override
        public Filter getFilter() {
            if (filter == null)
                filter = new CheeseFilter();
            return filter;
        }
        private class ViewHolder {
            FrameLayout mainFrame;
            ImageView userLogo, rightArrow;
            TextView userNameText, messageContentText;
            JSONObject jCategorie;

            public ViewHolder() {
                FrameLayout.LayoutParams mfp = new FrameLayout.LayoutParams(width, elementSize);
                mainFrame = new FrameLayout(activity);
                mainFrame.setLayoutParams(mfp);
                mainFrame.setBackgroundColor(Color.WHITE);

                FrameLayout.LayoutParams clp = new FrameLayout.LayoutParams(logoWidth, logoWidth);
                clp.leftMargin = width * 48 / 1080;
                clp.gravity = Gravity.CENTER_VERTICAL;
                userLogo = new ImageView(activity);
                userLogo.setLayoutParams(clp);

                mainFrame.addView(userLogo);

                FrameLayout.LayoutParams ctp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, elementSize/3);
                ctp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                userNameText = new TextView(activity);
                userNameText.setLayoutParams(ctp);
                userNameText.setPadding(0,height * 5 / 1920,0,0);
                userNameText.setGravity(Gravity.CENTER_VERTICAL);
                userNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.46f);
                userNameText.setTextColor(Color.rgb(64,64,64));
                mainFrame.addView(userNameText);

                FrameLayout.LayoutParams mcp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, elementSize - (elementSize/3));
                mcp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                mcp.topMargin = Utils.getBottom(userNameText);
                messageContentText = new TextView(activity);
                messageContentText.setLayoutParams(mcp);
                messageContentText.setGravity(Gravity.CENTER_VERTICAL);
                messageContentText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.41f);
                messageContentText.setTextColor(Color.rgb(154,154,154));
                messageContentText.setText("Merhabalar arkadaşlar bu mesaj bir test mesajıdır.");
                mainFrame.addView(messageContentText);

                FrameLayout.LayoutParams rap = new FrameLayout.LayoutParams(width * 40 / 1080, width * 40 / 1080);
                rap.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
                rap.rightMargin = width * 70 / 1080;
                rightArrow = new ImageView(activity);
                rightArrow.setBackgroundResource(R.drawable.right_arrow);
                rightArrow.setLayoutParams(rap);
                mainFrame.addView(rightArrow);
            }

            public void setData(JSONObject jCategorie) {
                this.jCategorie = jCategorie;
                try {
                    userNameText.setText(jCategorie.getString("userName"));
                    /*iconResId = getResources().getIdentifier(jNtfc.getString("image").toString(), "drawable", getContext().getPackageName());
                    userLogo.setImageResource(iconResId);*/
                    userLogo.setBackgroundResource(R.drawable.profile_icon);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        public UsersAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize);
            if (convertView == null) {
                convertView = new FrameLayout(activity);
                convertView.setLayoutParams(cp);

                holder = new ViewHolder();

                ((FrameLayout) convertView).addView(holder.mainFrame);
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            holder.setData(getItem(position));
            return convertView;
        }
    }
    public class CheeseFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase();
            FilterResults newFilterResults = new FilterResults();
            try {
                ArrayList<JSONObject> data = new ArrayList<JSONObject>();
                if (constraint != null && constraint.length() > 0) {
                    for (int i = 0; i < elements.size(); i++) {
                        JSONObject element = elements.get(i);
                        if (element.getString("userName").toLowerCase().contains(constraint))
                            data.add(element);
                    }
                    newFilterResults.count = data.size();
                    newFilterResults.values = data;
                } else {
                    newFilterResults.count = elements.size();
                    newFilterResults.values = elements;
                }
            } catch (Exception e) {
            }
            return newFilterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<JSONObject> resultData = (ArrayList<JSONObject>) results.values;
            adapter.clear();
            for (int i = 0; i < resultData.size(); i++) {
                adapter.add(resultData.get(i));
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void renderActionBar(final ActionBar actionBar) {
        final FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER_VERTICAL;
        tp.leftMargin = width * 50 / 1080;
        titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText("Mesajlar");
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.rgb(64,64,64));
        actionContainer.addView(titleText);

        FrameLayout.LayoutParams sip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        sip.gravity = Gravity.RIGHT;
        searchIcon = new ImageView(activity);
        searchIcon.setLayoutParams(sip);
        searchIcon.setImageResource(R.drawable.search_icon);
        searchIcon.setPadding((actionHeight / 3), (actionHeight / 3), (actionHeight / 3), (actionHeight / 3));
        searchIcon.setScaleType(ImageView.ScaleType.FIT_XY);
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                FrameLayout.LayoutParams sfp = new FrameLayout.LayoutParams(width - width * 20 / 1080, actionHeight - height * 20 / 1920);
                sfp.gravity = Gravity.CENTER;
                final FrameLayout searchFrame = new FrameLayout(activity);
                searchFrame.setLayoutParams(sfp);
                searchFrame.setBackgroundColor(Color.WHITE);

                GradientDrawable gd = new GradientDrawable();
                gd.setShape(GradientDrawable.RECTANGLE);
                gd.setColor(Color.WHITE);
                gd.setStroke(4, Color.rgb(211, 211, 211));
                searchFrame.setBackground(gd);

                FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(5 * actionHeight / 6, 5 * actionHeight / 6);
                bip.gravity = Gravity.CENTER_VERTICAL;
                bip.leftMargin = width * 50 / 1080;
                ImageButton backIcon = new ImageButton(activity);
                backIcon.setImageResource(R.drawable.search_back);
                backIcon.setLayoutParams(bip);
                backIcon.setPadding(actionHeight / 4, actionHeight / 4, actionHeight / 4, actionHeight / 4);
                backIcon.setScaleType(ImageView.ScaleType.FIT_XY);
                backIcon.setBackgroundColor(Color.WHITE);
                backIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        actionContainer.removeView(searchFrame);
                        Utils.hideKeyboard(activity);
                    }
                });

                searchFrame.addView(backIcon);

                FrameLayout.LayoutParams etp = new FrameLayout.LayoutParams(width - 2 * actionHeight - width * 100 / 1080, actionHeight);
                etp.gravity = Gravity.CENTER_VERTICAL;
                etp.leftMargin = Utils.getRight(backIcon);
                EditText searchEt = new EditText(activity);
                searchEt.setLayoutParams(etp);
                searchEt.setHint("Sohbet Ara..");
                searchEt.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.48f));
                searchEt.setTextColor(Color.GRAY);
                searchEt.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.getFilter().filter(s);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchFrame.addView(searchEt);

                FrameLayout.LayoutParams mp = new FrameLayout.LayoutParams(5 * actionHeight / 6, 5 * actionHeight / 6);
                mp.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
                mp.rightMargin = width * 60 / 1080;
                ImageButton microphoneIcon = new ImageButton(activity);
                microphoneIcon.setImageResource(R.drawable.microphone_icon);
                microphoneIcon.setLayoutParams(mp);
                microphoneIcon.setPadding(actionHeight / 5, actionHeight / 5, actionHeight / 5, actionHeight / 5);
                microphoneIcon.setScaleType(ImageView.ScaleType.FIT_XY);
                microphoneIcon.setBackgroundColor(Color.WHITE);
                microphoneIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                searchFrame.addView(microphoneIcon);

                actionContainer.addView(searchFrame);
            }
        });
        actionContainer.addView(searchIcon);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);


    }

    public void getUsers() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        remote.callAsync("GET", Constants.URL + Constants.URL_GET_USERS, params, "getMyFollowers", true);
    }

    @Override
    public void responsed(String method, final JSONObject response) {
        if (method.equals("getMyFollowers")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adapter.clear();
                        JSONArray users = response.getJSONArray("array");
                        for (int i = 0; i < users.length(); i++) {
                            adapter.add((JSONObject) users.get(i));
                            if (elementsStatus == true) {
                                elements.add((JSONObject) users.get(i));
                            }
                        }
                        elementsStatus = false;
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        }
    }

    @Override
    public void failed(String method, JSONObject response) {

    }
}

