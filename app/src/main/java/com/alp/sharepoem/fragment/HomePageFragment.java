package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;
import com.alp.sharepoem.view.EmptyView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class HomePageFragment extends BaseFragment implements Remotable,IActionBar{

    FrameLayout container;
    int width, height, sb, elementSize, logoWidth;
    int tabHeight, actionHeight, u;
    MainActivity activity;
    ListView listView;
    ProfileAdapter adapter;
    ImageButton addButton;
    int measureHeight;
    SwipyRefreshLayout swipeRefreshLayout;
    boolean isFirst = true;
    Typeface mediumFont,regularFont;
    Session session;
    JSONArray myFolloweds;
    JSONObject user = null;
    String categoryId;

    public HomePageFragment(Context context, String categoryId) {
        this.categoryId = categoryId;
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        tabHeight = (int) Utils.getTabHeight(activity);
        u = (int) Utils.getUnitSize(activity);
        actionHeight = (int) Utils.getActionHeight(activity);
        elementSize = height * 450 / 1920;
        logoWidth = width * 170 / 1080;
        mediumFont = Utils.getBoldFont(activity);
        regularFont = Utils.getRegularFont(activity);
        session = Session.getSession(activity);
        getMyFolloweds();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin=actionHeight;
        container.setLayoutParams(cp);
        container.setBackgroundColor(Color.WHITE);

        if (created){
            getMyFolloweds();
            getPosts();
            return;
        }
        render();
        getPosts();
        created = true;
    }

    public void render() {

        session.put("deviceToken", FirebaseInstanceId.getInstance().getToken());

        FrameLayout.LayoutParams sp  =  new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        //sp.topMargin = (int) actionHeight;
        swipeRefreshLayout = new SwipyRefreshLayout(activity);
        swipeRefreshLayout.setLayoutParams(sp);
        swipeRefreshLayout.setColorSchemeColors(Color.rgb(26, 41, 51));
        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                /*Log.d("MainActivity", "Refresh triggered at "
                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));*/
                isFirst = false;
                getPosts();
            }
        });

        FrameLayout.LayoutParams lvp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        //lvp.topMargin = (int) (height*15/1334+u);
        listView = new ListView(activity);
        listView.setLayoutParams(lvp);
        listView.setDividerHeight(0);
        adapter = new ProfileAdapter(activity);
        listView.setAdapter(adapter);
        listView.setDividerHeight(4);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*Intent intent = new Intent(getBaseContext(), PartActivity.class);
                intent.putExtra("part",adapter.getItem(position).toString());
                startActivity(intent);*/

            }
        });
        swipeRefreshLayout.addView(listView);
        container.addView(swipeRefreshLayout);



        FrameLayout.LayoutParams abp = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        abp.setMargins(0, 0, (int) u / 2, (int) u / 2);
        abp.gravity = Gravity.RIGHT | Gravity.BOTTOM;
        addButton = new ImageButton(activity);
        addButton.setLayoutParams(abp);
        addButton.setBackgroundResource(R.drawable.add_post);
        addButton.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View v) {
                //openAddActivity();
                SharePostsFragment sharePostsFragment = new SharePostsFragment(activity,categoryId);
                activity.addFragment(sharePostsFragment);
            }
        });
        container.addView(addButton);
    }


    public class ProfileAdapter extends ArrayAdapter<JSONObject> {

        private class ViewHolder {
            FrameLayout mainFrame,userInfoArea,line;
            TextView contentText, userNameText, timeText ,readArea;
            JSONObject jData;
            ImageView userLogo;
            FrameLayout.LayoutParams mfp, ctp;

            public ViewHolder(int position) {
                mfp = new FrameLayout.LayoutParams(width , elementSize);
                mfp.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
                mainFrame = new FrameLayout(getContext());
                mainFrame.setLayoutParams(mfp);
                mainFrame.setBackgroundColor(Color.WHITE);
                //mainFrame.setBackgroundResource(R.drawable.shadow_2749);

                FrameLayout.LayoutParams uiap = new FrameLayout.LayoutParams(width , logoWidth + height*20/1920);
                uiap.topMargin = height * 30 / 1920;
                userInfoArea = new FrameLayout(getContext());
                userInfoArea.setLayoutParams(uiap);
                userInfoArea.setBackgroundColor(Color.WHITE);

                FrameLayout.LayoutParams clp = new FrameLayout.LayoutParams(logoWidth, logoWidth);
                clp.leftMargin = width * 15 / 1080;
                userLogo = new ImageView(activity);
                userLogo.setLayoutParams(clp);
                userLogo.setBackgroundResource(R.drawable.profile_icon);

                userInfoArea.addView(userLogo);

                FrameLayout.LayoutParams untp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, logoWidth / 3);
                untp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                untp.topMargin = height * 15 / 1920;
                userNameText = new TextView(activity);
                userNameText.setLayoutParams(untp);
                userNameText.setPadding(0,height * 5 / 1920,0,0);
                userNameText.setGravity(Gravity.CENTER_VERTICAL);
                userNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.46f);
                userNameText.setTextColor(Color.BLACK);
                userNameText.setText("Abdullah Aydeniz");
                userNameText.setTypeface(regularFont);

                userInfoArea.addView(userNameText);

                FrameLayout.LayoutParams ttp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, logoWidth / 3);
                ttp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                ttp.topMargin = Utils.getBottom(userNameText) + height * 5 / 1920;
                timeText = new TextView(activity);
                timeText.setLayoutParams(ttp);
                timeText.setPadding(0,height * 5 / 1920,0,0);
                timeText.setGravity(Gravity.CENTER_VERTICAL);
                timeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.46f);
                timeText.setTextColor(Color.rgb(144,144,144));
                timeText.setText("Dün 10:38'de");

                userInfoArea.addView(timeText);

                mainFrame.addView(userInfoArea);

                ctp = new FrameLayout.LayoutParams(width - width * 40 / 1080 , elementSize/2);
                ctp.topMargin = Utils.getBottom(userInfoArea);
                ctp.leftMargin = width * 20 / 1080;
                contentText = new TextView(getContext());
                contentText.setLayoutParams(ctp);
                contentText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(getContext()) * 5 / 8) * 0.50f);
                contentText.setGravity(Gravity.CENTER_VERTICAL);
                contentText.setTextColor(Color.BLACK);

                mainFrame.addView(contentText);


                FrameLayout.LayoutParams rap = new FrameLayout.LayoutParams(width , height * 145 / 1920);
                rap.gravity = Gravity.BOTTOM;
                rap.topMargin = Utils.getBottom(contentText);
                readArea = new TextView(getContext());
                readArea.setLayoutParams(rap);
                readArea.setBackgroundColor(Color.WHITE);
                readArea.setGravity(Gravity.CENTER);
                readArea.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(getContext()) * 6 / 8) * 0.50f);
                readArea.setTextColor(Color.rgb(73,157,255));
                readArea.setTypeface(mediumFont);
                readArea.setText("Oku");
                readArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            PostPartFragment postPartFragment = new PostPartFragment(activity,adapter.getItem(adapter.getPosition(jData)).getString("title"),adapter.getItem(adapter.getPosition(jData)).getString("root"),adapter.getItem(adapter.getPosition(jData)).getString("id"));
                            activity.addFragment(postPartFragment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                mainFrame.addView(readArea);

                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width , height * 4 / 1920);
                lp.gravity = Gravity.BOTTOM;
                lp.bottomMargin = Utils.getTop(readArea);
                line = new FrameLayout(getContext());
                line.setLayoutParams(lp);
                line.setBackgroundColor(Color.rgb(209,209,209));

                mainFrame.addView(line);
            }

            public void setData(JSONObject jData, View view) {
                this.jData = jData;
                try {
                    userNameText.setText(jData.getString("name"));
                    timeText.setText("@"+jData.getString("userName"));
                    contentText.setText(jData.getString("firstPart"));
                    int size = (int) contentText.getTextSize();
                    measureHeight = getHeight(getContext(), contentText.getText().toString(),size,width*686/750);
                    mfp.height = measureHeight+elementSize;
                    ctp.height = measureHeight;
                    //schoolName.setText(jData.getString("content"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        public ProfileAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = new FrameLayout(getContext());

                holder = new ViewHolder(position);

                ((FrameLayout) convertView).addView(holder.mainFrame);
                /*AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize+height*40/1334);
                convertView.setLayoutParams(cp);*/
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            holder.setData(getItem(position),convertView);
            AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, measureHeight+elementSize);
            //cp.height = measureHeight + height*40/1334;
            convertView.setLayoutParams(cp);
            return convertView;
        }
    }

    public static int getHeight(Context context, String text, int textSize, int deviceWidth) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public void getPosts(){
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        remote.callAsync("POST", Constants.URL + Constants.URL_GET_POSTS_FOR_CATEGORIES + "/" + categoryId, params, "getPosts", true);
    }

    @Override
    public void responsed(String method, final JSONObject response) {
        if (method.equals("getPosts")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isFirst){

                    }
                        //activity.dismisLoadingActivity();
                    else
                        swipeRefreshLayout.setRefreshing(false);
                    try {
                        adapter.clear();
                        JSONArray posts = response.getJSONArray("array");
                        for (int i = posts.length()-1 ; i >= 0 ; i--) {
                            for (int j = 0 ; j <= myFolloweds.length() ; j++) {
                                if (posts.getJSONObject(i).getString("userId").equals(myFolloweds.getJSONObject(j).getString("followedId")) ||
                                        user.getString("id").equals(posts.getJSONObject(i).getString("userId"))) {
                                    adapter.add((JSONObject) posts.get(i));
                                    break;
                                }
                            }
                        }
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        }else if (method.equals("getMyFolloweds")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        myFolloweds = response.getJSONArray("array");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        }
    }

    @Override
    public void failed(String method, JSONObject response) {

    }

    public void updateAdapter(JSONObject response, String status){

    }

    public void getMyFolloweds() {
        //activity.showLoadingActivity();
        try {
            user = new JSONObject(session.getString("user"));
            Remote remote = new Remote(activity, this);
            JSONObject params = new JSONObject();

            remote.callAsync("POST", Constants.URL + Constants.URL_GET_MY_FOLLOWEDS + "/" + user.getString("id"), params, "getMyFolloweds", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void renderActionBar(ActionBar actionBar) {
        FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        ImageView backIcon = new ImageView(activity);
        backIcon.setLayoutParams(bip);
        backIcon.setImageResource(R.drawable.back_icon);
        backIcon.setPadding((actionHeight / 3), (actionHeight / 3), (actionHeight / 3), (actionHeight / 3));
        backIcon.setScaleType(ImageView.ScaleType.FIT_XY);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
        actionContainer.addView(backIcon);

        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        tp.leftMargin = /*Utils.getRight(settingsIv) + */width * 15 / 1080;
        TextView titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText("Ana Sayfa");
        titleText.setTypeface(mediumFont);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        actionContainer.addView(titleText);

        /*FrameLayout.LayoutParams cip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        cip.gravity = Gravity.RIGHT;
        ImageView carModeIv = new ImageView(activity);
        carModeIv.setLayoutParams(cip);
        carModeIv.setImageResource(R.drawable.car_mode_icon);
        carModeIv.setPadding((actionHeight / 4), (actionHeight / 4), (actionHeight / 4), (actionHeight / 4));
        carModeIv.setScaleType(ImageView.ScaleType.FIT_XY);
        carModeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.getPlayBar().isVisible()) {
                    CarModeFragment carModeFragment = new CarModeFragment(activity);
                    activity.addFragment(carModeFragment);
                }
            }
        });
        actionContainer.addView(carModeIv);*/

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);
    }
}

