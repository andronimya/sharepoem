package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;
import com.alp.sharepoem.view.EmptyView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


@SuppressLint("ValidFragment")
public class FollowFragment extends Fragment implements IActionBar,Remotable {
    FrameLayout container, lastSearchedFrame;
    int width, height, sb, actionHeight, tabHeight, elementSize, logoWidth;
    MainActivity activity;
    Session session;
    boolean created = false;
    ListView listView;
    TextView titleText, lastSearchTitle;
    ImageView searchIcon;
    Typeface mediumfont;
    EditText searchEt;
    FrameLayout.LayoutParams lvp;
    UsersAdapter adapter;
    EmptyView emptyView;
    ArrayList<JSONObject> elements;
    boolean elementsStatus = true;
    String userId;
    String followStatus;

    public FollowFragment(Context context, String userId,String followStatus) {
        this.followStatus = followStatus;
        this.userId = userId;
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        actionHeight = (int) Utils.getActionHeight(activity);
        tabHeight = (int) Utils.getTabHeight(activity);
        elementSize = height * 170 / 1920;
        session = Session.getSession(activity);
        elements = new ArrayList<>();
        logoWidth = width * 130 / 1080;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created){
            container = new FrameLayout(getActivity());
        }

        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin = actionHeight;
        container.setLayoutParams(cp);
        //container.setBackgroundColor(Color.BLUE);

        render();
        created = true;
    }

    public void render() {
        lvp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        listView = new ListView(activity);
        listView.setLayoutParams(lvp);
        listView.setDividerHeight(0);
        listView.setFocusable(false);
        adapter = new UsersAdapter(activity);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        container.addView(listView);

        if (followStatus.equals("follower"))
            getMyFollowers();
        else
            getMyFolloweds();
    }


    public class UsersAdapter extends ArrayAdapter<JSONObject> {
        Filter filter;

        @Override
        public Filter getFilter() {
            if (filter == null)
                filter = new CheeseFilter();
            return filter;
        }
        private class ViewHolder {
            FrameLayout mainFrame;
            ImageView userLogo, rightArrow;
            TextView userNameText;
            JSONObject jUser;

            public ViewHolder() {
                FrameLayout.LayoutParams mfp = new FrameLayout.LayoutParams(width, elementSize);
                mainFrame = new FrameLayout(activity);
                mainFrame.setLayoutParams(mfp);
                mainFrame.setBackgroundColor(Color.WHITE);
                mainFrame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*ProfileFragment profileFragment = new ProfileFragment(activity,true,adapter.getItem(adapter.getPosition(jNtfc)));
                        activity.addFragment(profileFragment);*/
                    }
                });

                FrameLayout.LayoutParams clp = new FrameLayout.LayoutParams(logoWidth, logoWidth);
                clp.leftMargin = width * 48 / 1080;
                clp.gravity = Gravity.CENTER_VERTICAL;
                userLogo = new ImageView(activity);
                userLogo.setLayoutParams(clp);

                mainFrame.addView(userLogo);

                FrameLayout.LayoutParams ctp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, elementSize);
                ctp.gravity = Gravity.CENTER_VERTICAL;
                ctp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                userNameText = new TextView(activity);
                userNameText.setLayoutParams(ctp);
                userNameText.setGravity(Gravity.CENTER_VERTICAL);
                userNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.48f);
                userNameText.setTextColor(Color.BLACK);
                mainFrame.addView(userNameText);

                FrameLayout.LayoutParams rap = new FrameLayout.LayoutParams(width * 40 / 1080, width * 40 / 1080);
                rap.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
                rap.rightMargin = width * 70 / 1080;
                rightArrow = new ImageView(activity);
                rightArrow.setBackgroundResource(R.drawable.right_arrow);
                rightArrow.setLayoutParams(rap);
                mainFrame.addView(rightArrow);
            }

            public void setData(JSONObject jUser) {
                this.jUser = jUser;
                try {
                    if(followStatus.equals("follower"))
                        userNameText.setText(jUser.getString("followerName"));
                    else
                        userNameText.setText(jUser.getString("followedName"));
                    /*iconResId = getResources().getIdentifier(jNtfc.getString("image").toString(), "drawable", getContext().getPackageName());
                    userLogo.setImageResource(iconResId);*/
                    userLogo.setBackgroundResource(R.drawable.profile_icon);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        public UsersAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize);
            if (convertView == null) {
                convertView = new FrameLayout(activity);
                convertView.setLayoutParams(cp);

                holder = new ViewHolder();

                ((FrameLayout) convertView).addView(holder.mainFrame);
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            holder.setData(getItem(position));
            return convertView;
        }
    }
    public class CheeseFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase();
            FilterResults newFilterResults = new FilterResults();
            try {
                ArrayList<JSONObject> data = new ArrayList<JSONObject>();
                if (constraint != null && constraint.length() > 0) {
                    for (int i = 0; i < elements.size(); i++) {
                        JSONObject element = elements.get(i);
                        if (element.getString("userName").toLowerCase().contains(constraint))
                            data.add(element);
                    }
                    newFilterResults.count = data.size();
                    newFilterResults.values = data;
                } else {
                    newFilterResults.count = elements.size();
                    newFilterResults.values = elements;
                }
            } catch (Exception e) {
            }
            return newFilterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<JSONObject> resultData = (ArrayList<JSONObject>) results.values;
            adapter.clear();
            for (int i = 0; i < resultData.size(); i++) {
                adapter.add(resultData.get(i));
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void renderActionBar(final ActionBar actionBar) {
        FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        ImageView backIcon = new ImageView(activity);
        backIcon.setLayoutParams(bip);
        backIcon.setImageResource(R.drawable.back_icon);
        backIcon.setPadding((actionHeight / 3), (actionHeight / 3), (actionHeight / 3), (actionHeight / 3));
        backIcon.setScaleType(ImageView.ScaleType.FIT_XY);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
        actionContainer.addView(backIcon);

        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        tp.leftMargin = /*Utils.getRight(settingsIv) + */width * 15 / 1080;
        TextView titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        if (followStatus.equals("follower"))
            titleText.setText("Takipçiler");
        else
            titleText.setText("Takip Edilenler");
        titleText.setTypeface(mediumfont);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        actionContainer.addView(titleText);

        /*FrameLayout.LayoutParams cip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        cip.gravity = Gravity.RIGHT;
        ImageView carModeIv = new ImageView(activity);
        carModeIv.setLayoutParams(cip);
        carModeIv.setImageResource(R.drawable.car_mode_icon);
        carModeIv.setPadding((actionHeight / 4), (actionHeight / 4), (actionHeight / 4), (actionHeight / 4));
        carModeIv.setScaleType(ImageView.ScaleType.FIT_XY);
        carModeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.getPlayBar().isVisible()) {
                    CarModeFragment carModeFragment = new CarModeFragment(activity);
                    activity.addFragment(carModeFragment);
                }
            }
        });
        actionContainer.addView(carModeIv);*/

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);

    }

    public void getMyFollowers() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        remote.callAsync("POST", Constants.URL + Constants.URL_GET_MY_FOLLOWERS + "/" + userId, params, "getMyFollowers", true);
    }

    public void getMyFolloweds() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        remote.callAsync("POST", Constants.URL + Constants.URL_GET_MY_FOLLOWEDS + "/" + userId, params, "getMyFolloweds", true);
    }

    @Override
    public void responsed(String method, final JSONObject response) {
        if (method.equals("getMyFollowers")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adapter.clear();
                        JSONArray users = response.getJSONArray("array");
                        for (int i = 0; i < users.length(); i++) {
                            adapter.add((JSONObject) users.get(i));
                            if (elementsStatus == true) {
                                elements.add((JSONObject) users.get(i));
                            }
                        }
                        elementsStatus = false;
                        adapter.notifyDataSetChanged();
                        if (adapter.getCount() == 0) {
                            emptyView = new EmptyView(activity, "search_empty_view",
                                    "Takipçiler", "Takipçiniz bulunmamaktadır.");
                            container.addView(emptyView);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        }else if (method.equals("getMyFolloweds")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adapter.clear();
                        JSONArray users = response.getJSONArray("array");
                        for (int i = 0; i < users.length(); i++) {
                            adapter.add((JSONObject) users.get(i));
                            if (elementsStatus == true) {
                                elements.add((JSONObject) users.get(i));
                            }
                        }
                        elementsStatus = false;
                        adapter.notifyDataSetChanged();
                        if (adapter.getCount() == 0) {
                            emptyView = new EmptyView(activity, "search_empty_view",
                                    "Takip Edilenler", "Kimseyi Takip Etmiyorsunuz..");
                            container.addView(emptyView);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        }
    }

    @Override
    public void failed(String method, JSONObject response) {

    }
}

