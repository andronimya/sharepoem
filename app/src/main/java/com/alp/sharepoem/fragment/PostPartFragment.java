package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class PostPartFragment extends BaseFragment implements Remotable,IActionBar{

    FrameLayout container;
    int width, height, sb, elementSize, logoWidth;
    int tabHeight, actionHeight, u;
    MainActivity activity;
    ListView listView;
    ProfileAdapter adapter;
    ImageButton addButton;
    int measureHeight;
    String title,root, postId;
    JSONArray parts;
    ArrayList<JSONObject> postParts;
    Typeface mediumFont,regularFont;

    public PostPartFragment(Context context, String title, String root, String postId) {
        this.title = title;
        this.root = root;
        this.postId = postId;
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        tabHeight = (int) Utils.getTabHeight(activity);
        u = (int) Utils.getUnitSize(activity);
        actionHeight = (int) Utils.getActionHeight(activity);
        elementSize = height * 600 / 1920;
        logoWidth = width * 170 / 1080;
        mediumFont = Utils.getBoldFont(activity);
        regularFont = Utils.getRegularFont(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(false);
        activity.setCurrentFragment(this);

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight));
        cp.topMargin=actionHeight;
        container.setLayoutParams(cp);
        container.setBackgroundColor(Color.WHITE);

        if (created) {
            getParts();
            return;
        }
        render();
        getParts();
        created = true;
    }

    public void render() {
        FrameLayout.LayoutParams lvp = new FrameLayout.LayoutParams(width, height - (actionHeight));
        //lvp.topMargin = (int) (height*15/1334+u);
        listView = new ListView(activity);
        listView.setLayoutParams(lvp);
        listView.setDividerHeight(0);
        adapter = new ProfileAdapter(activity);
        listView.setAdapter(adapter);
        listView.setDividerHeight(4);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        container.addView(listView);
    }

    public class ProfileAdapter extends ArrayAdapter<JSONObject> {

        private class ViewHolder {
            FrameLayout mainFrame,userInfoArea,line,rightAndLeftIcon,addPartArea;
            TextView contentText, userNameText, timeText, sequenceText;
            JSONObject jData;
            ImageView userLogo,addPart;
            FrameLayout.LayoutParams mfp, ctp;
            ImageView leftIcon, rightIcon;

            public ViewHolder(int position) {
                mfp = new FrameLayout.LayoutParams(width , elementSize);
                mfp.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
                mainFrame = new FrameLayout(getContext());
                mainFrame.setLayoutParams(mfp);
                mainFrame.setBackgroundColor(Color.WHITE);
                //mainFrame.setBackgroundResource(R.drawable.shadow_2749);

                FrameLayout.LayoutParams uiap = new FrameLayout.LayoutParams(width , logoWidth + height*20/1920);
                userInfoArea = new FrameLayout(getContext());
                userInfoArea.setLayoutParams(uiap);
                userInfoArea.setBackgroundColor(Color.WHITE);

                FrameLayout.LayoutParams clp = new FrameLayout.LayoutParams(logoWidth, logoWidth);
                clp.leftMargin = width * 15 / 1080;
                userLogo = new ImageView(activity);
                userLogo.setLayoutParams(clp);
                userLogo.setBackgroundResource(R.drawable.profile_icon);

                userInfoArea.addView(userLogo);

                FrameLayout.LayoutParams untp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, logoWidth / 3);
                untp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                untp.topMargin = height * 15 / 1920;
                userNameText = new TextView(activity);
                userNameText.setLayoutParams(untp);
                userNameText.setPadding(0,height * 5 / 1920,0,0);
                userNameText.setGravity(Gravity.CENTER_VERTICAL);
                userNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.46f);
                userNameText.setTextColor(Color.BLACK);
                userNameText.setText("Abdullah Aydeniz");
                //userNameText.setTypeface(regularFont);

                userInfoArea.addView(userNameText);

                FrameLayout.LayoutParams ttp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, logoWidth / 3);
                ttp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                ttp.topMargin = Utils.getBottom(userNameText) + height * 5 / 1920;
                timeText = new TextView(activity);
                timeText.setLayoutParams(ttp);
                timeText.setPadding(0,height * 5 / 1920,0,0);
                timeText.setGravity(Gravity.CENTER_VERTICAL);
                timeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.46f);
                timeText.setTextColor(Color.rgb(144,144,144));
                timeText.setText("Dün 10:38'de");

                userInfoArea.addView(timeText);

                mainFrame.addView(userInfoArea);

                ctp = new FrameLayout.LayoutParams(width - width * 40 / 1080 , elementSize/2);
                ctp.topMargin = Utils.getBottom(userInfoArea);
                ctp.leftMargin = width * 20 / 1080;
                contentText = new TextView(getContext());
                contentText.setLayoutParams(ctp);
                contentText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(getContext()) * 5 / 8) * 0.50f);
                contentText.setGravity(Gravity.CENTER_VERTICAL);
                contentText.setTextColor(Color.BLACK);

                mainFrame.addView(contentText);

                FrameLayout.LayoutParams ralap = new FrameLayout.LayoutParams(width , height * 220 / 1920);
                ralap.gravity = Gravity.BOTTOM;
                rightAndLeftIcon = new FrameLayout(getContext());
                rightAndLeftIcon.setLayoutParams(ralap);

                FrameLayout.LayoutParams lip = new FrameLayout.LayoutParams(logoWidth / 2 , logoWidth / 2);
                lip.leftMargin = width * 100/1080;
                lip.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
                leftIcon = new ImageView(getContext());
                leftIcon.setLayoutParams(lip);
                leftIcon.setBackgroundResource(R.drawable.left_icon);
                leftIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            nextPart(adapter.getItem(adapter.getPosition(jData)).getString("parent"),Integer.parseInt(adapter.getItem(adapter.getPosition(jData)).getString("sequence"))-1,adapter.getPosition(jData));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                rightAndLeftIcon.addView(leftIcon);

                FrameLayout.LayoutParams seqp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, logoWidth / 3);
                seqp.gravity = Gravity.CENTER;
                sequenceText = new TextView(activity);
                sequenceText.setLayoutParams(seqp);
                sequenceText.setGravity(Gravity.CENTER);
                sequenceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8) * 0.46f);
                sequenceText.setTextColor(Color.rgb(26,41,51));
                sequenceText.setTypeface(mediumFont);

                rightAndLeftIcon.addView(sequenceText);

                FrameLayout.LayoutParams rip = new FrameLayout.LayoutParams(logoWidth / 2 , logoWidth / 2);
                rip.rightMargin = width * 100/1080;
                rip.gravity = Gravity.CENTER_VERTICAL | Gravity.RIGHT;
                rightIcon = new ImageView(getContext());
                rightIcon.setLayoutParams(rip);
                rightIcon.setBackgroundResource(R.drawable.right_icon);
                rightIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            nextPart(adapter.getItem(adapter.getPosition(jData)).getString("parent"),Integer.parseInt(adapter.getItem(adapter.getPosition(jData)).getString("sequence"))+1,adapter.getPosition(jData));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                rightAndLeftIcon.addView(rightIcon);

                mainFrame.addView(rightAndLeftIcon);



                FrameLayout.LayoutParams rap = new FrameLayout.LayoutParams(width , height * 145 / 1920);
                rap.gravity = Gravity.BOTTOM;
                rap.bottomMargin = Utils.getTop(rightAndLeftIcon);
                addPartArea = new FrameLayout(activity);
                addPartArea.setLayoutParams(rap);
                addPartArea.setBackgroundColor(Color.rgb(52,82,102));
                addPartArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            SharePostPartFragment sharePostPartFragment = new SharePostPartFragment(activity, postId,adapter.getItem(adapter.getPosition(jData)).getString("id"));
                            activity.addFragment(sharePostPartFragment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                FrameLayout.LayoutParams app = new FrameLayout.LayoutParams(logoWidth / 2, logoWidth / 2);
                app.gravity = Gravity.CENTER;
                addPart = new ImageView(activity);
                addPart.setLayoutParams(app);
                addPart.setBackgroundResource(R.drawable.add_part);

                addPartArea.addView(addPart);



                mainFrame.addView(addPartArea);

                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width , height * 4 / 1920);
                lp.gravity = Gravity.BOTTOM;
                lp.bottomMargin = Utils.getTop(addPartArea);
                line = new FrameLayout(activity);
                line.setLayoutParams(lp);
                line.setBackgroundColor(Color.rgb(209,209,209));

                mainFrame.addView(line);

            }

            public void setData(JSONObject jData, View view) {
                this.jData = jData;
                try {
                    contentText.setText(jData.getString("text"));
                    userNameText.setText(jData.getString("name"));
                    timeText.setText("@"+jData.getString("userName"));
                    int size = (int) contentText.getTextSize();
                    measureHeight = getHeight(getContext(), contentText.getText().toString(),size,width*686/750);
                    mfp.height = measureHeight+elementSize;
                    ctp.height = measureHeight;


                    if(jData.getString("status").equals("1")){
                        leftIcon.setVisibility(View.INVISIBLE);
                        rightIcon.setVisibility(View.INVISIBLE);
                    }else{
                        leftIcon.setVisibility(View.VISIBLE);
                        rightIcon.setVisibility(View.VISIBLE);
                    }
                    if(jData.getString("sequence").equals("0")){
                        leftIcon.setVisibility(View.INVISIBLE);
                    }
                    int sequence=0;
                    if(jData.getString("status").equals("0")){//aynı parent idye sahip kaç tane var
                        sequence = getSequence(jData.getString("parent"))-1;
                    }

                    if(jData.getString("sequence").equals(String.valueOf(sequence))){//yani eğer bizim toplam sequence sayımız 0(-1 den dolayı) ise  görünmeyecek
                        rightIcon.setVisibility(View.INVISIBLE);
                    }

                    sequenceText.setText(String.valueOf(Integer.parseInt(jData.getString("sequence"))+1)+"/" + String.valueOf(sequence+1));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        public ProfileAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = new FrameLayout(getContext());

                holder = new ViewHolder(position);

                ((FrameLayout) convertView).addView(holder.mainFrame);
                /*AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize+height*40/1334);
                convertView.setLayoutParams(cp);*/
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            holder.setData(getItem(position),convertView);
            AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, measureHeight+elementSize);
            //cp.height = measureHeight + height*40/1334;
            convertView.setLayoutParams(cp);
            return convertView;
        }
    }

    public static int getHeight(Context context, String text, int textSize, int deviceWidth) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public void getParts(){
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        remote.callAsync("POST", Constants.URL + Constants.URL_GET_PARTS_OF_POST + "/" + postId, params, "getParts", true);
    }


    @Override
    public void responsed(String method, final JSONObject response) {
        if (method.equals("getParts")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adapter.clear();
                        parts = response.getJSONArray("array");
                        postParts = new ArrayList<JSONObject>();
                        for (int i = 0; i < parts.length(); i++) {
                            /*if (parts.getJSONObject(i).getString("sequence").equals("0"))
                                adapter.add((JSONObject) parts.get(i));*/
                            if (parts.getJSONObject(i).getString("id").equals(root)){
                                //adapter.add((JSONObject) parts.get(i));
                                postParts.add(parts.getJSONObject(i));
                                getPostPartChild(root);
                                addAdapter(postParts);
                                break;
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    adapter.notifyDataSetChanged();

                    //activity.dismisLoadingActivity();
                }
            });
        }
    }

    @Override
    public void failed(String method, JSONObject response) {
        try {
            response.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    ArrayList<JSONObject> nextParts;
    public void nextPart(String parent, int sequence, int position){
        nextParts = new ArrayList<JSONObject>();
        for (int i = 0 ; i< parts.length() ; i++){
            try {
                if (parts.getJSONObject(i).getString("parent").equals(parent) && Integer.parseInt(parts.getJSONObject(i).getString("sequence")) == sequence){
                    /*adapter.clear();
                    adapter.add(parts.getJSONObject(i));*/
                    nextParts.add(parts.getJSONObject(i));//sonraki part için oluşturduğumuz arrayin içini dolduruyoruz
                    getNextParts(parts.getJSONObject(i).getString("id"));//ilkini ekleyip diğerlerini eklemesi için bu methodu çağırıyoruz
                    changeAdapter(nextParts);
                    //adapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void getNextParts(String parent){//sürekli kendinin id sine göre parent idleri arayıp onların sequencesi 0 olanları getiriyor

        String parentId = null;
        boolean childStatus = false;//döngünün durması için önemli
        for (int i = 0 ; i< parts.length() ; i++){
            try {
                if (parts.getJSONObject(i).getString("parent").equals(parent)
                        && parts.getJSONObject(i).getString("sequence").equals("0")){
                    //adapter.add(parts.getJSONObject(i));
                    nextParts.add(parts.getJSONObject(i));
                    parentId = parts.getJSONObject(i).getString("id");
                    childStatus = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (childStatus)
            getNextParts(parentId);
        else
            return;
    }

    public int getSequence(String parentId){
        int sequenceCount = 0;
        for (int i = 0 ; i< parts.length() ; i++){
            try {
                if (parts.getJSONObject(i).getString("parent").equals(parentId)){
                    sequenceCount++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return sequenceCount;
    }


    public void getPostPartChild(String parent){//sürekli kendinin id sine göre parent idleri arayıp onların sequencesi 0 olanları getiriyor
        String parentId = null;
        boolean childStatus = false;//döngünün durması için önemli
        for (int i = 0 ; i< parts.length() ; i++){
            try {
                if (parts.getJSONObject(i).getString("parent").equals(parent)
                        && parts.getJSONObject(i).getString("sequence").equals("0")){
                    //adapter.add(parts.getJSONObject(i));
                    postParts.add(parts.getJSONObject(i));
                    parentId = parts.getJSONObject(i).getString("id");
                    childStatus = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (childStatus)
            getPostPartChild(parentId);
        else
            return;
    }



    public void changeAdapter(ArrayList<JSONObject> parts){
        try {
            JSONObject changePart = parts.get(0);// bu benim değişecek olan partım elimdeki arrayın ilki
            int i=0;
            for(JSONObject pp : postParts){
                if(changePart.getString("parent").equals(pp.getString("id"))){
                    postParts.subList(0,i);
                    //değişecek olan partıma kadar elimdeki tüm partları(postPart) kontrol ediyorum eğer onun
                    // parenti ile diğerinin id si aynı ise oraya kadar olanları alıyorum subList o işe yarıyor geri kalanında ise elimdeki kalan postPartın üzerine nextPart
                    // olarak gönderdiğim partı aşağıda birleştiriyorum(üzerine ekliyorum) sonra da tekrar addAdapter metodunu çağırıp elimdeki arrayi ekliyorum
                    break;
                }
                i++;
            }

            for(int j = postParts.size()-1 ; j>i ; j--){
                postParts.remove(j);
            }

            postParts.addAll(parts);
            addAdapter(postParts);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addAdapter(ArrayList<JSONObject> postParts){
        adapter.clear();
        for (int i = 0;i < postParts.size() ; i++){
            adapter.add(postParts.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void renderActionBar(final ActionBar actionBar) {
        FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        ImageView backIcon = new ImageView(activity);
        backIcon.setLayoutParams(bip);
        backIcon.setImageResource(R.drawable.back_icon);
        backIcon.setPadding((actionHeight / 3), (actionHeight / 3), (actionHeight / 3), (actionHeight / 3));
        backIcon.setScaleType(ImageView.ScaleType.FIT_XY);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
        actionContainer.addView(backIcon);

        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        tp.leftMargin = /*Utils.getRight(settingsIv) + */width * 15 / 1080;
        TextView titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText(title);
        //titleText.setTypeface(mediumfont);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        actionContainer.addView(titleText);

        /*FrameLayout.LayoutParams cip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        cip.gravity = Gravity.RIGHT;
        ImageView carModeIv = new ImageView(activity);
        carModeIv.setLayoutParams(cip);
        carModeIv.setImageResource(R.drawable.car_mode_icon);
        carModeIv.setPadding((actionHeight / 4), (actionHeight / 4), (actionHeight / 4), (actionHeight / 4));
        carModeIv.setScaleType(ImageView.ScaleType.FIT_XY);
        carModeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.getPlayBar().isVisible()) {
                    CarModeFragment carModeFragment = new CarModeFragment(activity);
                    activity.addFragment(carModeFragment);
                }
            }
        });
        actionContainer.addView(carModeIv);*/

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);
    }

}

