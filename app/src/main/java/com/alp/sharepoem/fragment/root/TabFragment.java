package com.alp.sharepoem.fragment.root;

import android.support.v4.app.Fragment;

import com.alp.sharepoem.activity.MainActivity;


public abstract class TabFragment extends Fragment {

    @Override
    public void onResume(){
        ((MainActivity) getActivity()).activeFragment=this;
        super.onResume();
    }

    public abstract void addFragment(Fragment fragment);

    public abstract void onBackPressed();

    public abstract void clearStackFragment();

}