package com.alp.sharepoem.fragment;


import android.support.v4.app.Fragment;


public class BaseFragment extends Fragment {
    Boolean created = false;

    public Boolean getCreated() {
        return created;
    }

    public void setCreated(Boolean created) {
        this.created = created;
    }
}
