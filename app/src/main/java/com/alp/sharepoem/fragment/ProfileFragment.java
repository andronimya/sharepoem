package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.LoginActivity;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class ProfileFragment extends Fragment implements IActionBar,Remotable {
    FrameLayout container, profileBg, followArea;
    int width, height, sb, actionHeight, tabHeight, logoSize, logoWidth,elementSize,measureHeight,logoWidthForImage;
    MainActivity activity;
    Session session;
    boolean created = false;
    Typeface mediumfont;
    JSONObject userInfo;
    ImageView userLogo;
    TextView followText, followersText, nameText, followTv;
    ProfileAdapter adapter;
    ListView listView;
    Typeface mediumFont,regularFont;
    boolean isSearch = false;
    JSONObject userInfoForSearch, activeFriendProfile;
    JSONArray myFolloweds, myFollowers;

    public ProfileFragment(Context context,boolean isSearch,JSONObject userInfoForSearch) {
        this.userInfoForSearch = userInfoForSearch;
        this.isSearch = isSearch;
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        actionHeight = (int) Utils.getActionHeight(activity);
        tabHeight = (int) Utils.getTabHeight(activity);
        logoSize = width * 140 / 1080;
        session = Session.getSession(activity);
        //logoWidth = width * 250 / 1080;
        elementSize = height * 450 / 1920;
        logoWidth = width * 170 / 1080;
        logoWidthForImage = width * 280 / 1080;
        mediumFont = Utils.getBoldFont(activity);
        regularFont = Utils.getRegularFont(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);

        if (!isSearch){
            try {
                userInfo = new JSONObject(session.getString("user"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            userInfo = userInfoForSearch;
        }
        getMyFolloweds();
        getMyFollowers();


        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin = actionHeight;
        container.setLayoutParams(cp);

    }

    public void render() {
        FrameLayout.LayoutParams pbgp = new FrameLayout.LayoutParams(width, height * 450 / 1920);
        profileBg = new FrameLayout(activity);
        profileBg.setLayoutParams(pbgp);
        profileBg.setBackgroundColor(Color.rgb(26,41,51));

        FrameLayout.LayoutParams clp = new FrameLayout.LayoutParams(logoWidthForImage, logoWidthForImage);
        clp.topMargin = height * 30 / 1920;
        clp.gravity = Gravity.CENTER_HORIZONTAL;
        userLogo = new ImageView(activity);
        userLogo.setLayoutParams(clp);
        userLogo.setBackgroundResource(R.drawable.profile_icon);

        profileBg.addView(userLogo);

        FrameLayout.LayoutParams unp = new FrameLayout.LayoutParams(width/2, height * 150 / 1920);
        unp.gravity = Gravity.CENTER_HORIZONTAL;
        unp.topMargin = Utils.getBottom(userLogo);
        nameText = new TextView(activity);
        nameText.setLayoutParams(unp);
        nameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8) * 0.46f);
        nameText.setTextColor(Color.WHITE);
        nameText.setGravity(Gravity.CENTER_HORIZONTAL);
        try {
            nameText.setText(userInfo.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        profileBg.addView(nameText);

        if (isSearch) {
            FrameLayout.LayoutParams ip = new FrameLayout.LayoutParams(width * 220 / 1080, height * 100 / 1920);
            ip.gravity = Gravity.BOTTOM | Gravity.RIGHT;
            ip.rightMargin =width * 50 / 4080;
            ip.bottomMargin = height * 30 / 1920;
            followTv = new TextView(activity);
            followTv.setLayoutParams(ip);
            followTv.setTextColor(Color.BLACK);
            followTv.setGravity(Gravity.CENTER);
            followTv.setTypeface(regularFont);
            if (isFollowed()) {
                followTv.setText("Takipten Çık");
            } else
                followTv.setText("Takip Et");
            followTv.setBackground(getGd());
            followTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getViewHeight(followTv) * 0.52f));
            followTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isFollowed()) {
                        removeFolloweds();
                    } else {
                        addFollow();
                    }
                    getMyFolloweds();
                    getMyFollowers();
                }
            });
            profileBg.addView(followTv);
        }

        container.addView(profileBg);

        FrameLayout.LayoutParams fap = new FrameLayout.LayoutParams(width , height * 180 / 1920);
        fap.topMargin = Utils.getBottom(profileBg);
        followArea = new FrameLayout(activity);
        followArea.setLayoutParams(fap);
        followArea.setBackgroundColor(Color.WHITE);


        FrameLayout.LayoutParams ftp = new FrameLayout.LayoutParams(width/2, height * 150 / 1920);
        followText = new TextView(activity);
        followText.setLayoutParams(ftp);
        followText.setGravity(Gravity.CENTER);
        followText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8) * 0.46f);
        followText.setTextColor(Color.BLACK);
        followText.setText("Takip\n" + myFolloweds.length());
        followText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FollowFragment followFragment = new FollowFragment(activity,userInfo.getString("id"),"followed");
                    activity.addFragment(followFragment);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        followArea.addView(followText);

        FrameLayout.LayoutParams lvp = new FrameLayout.LayoutParams(width * 5 / 1080,height * 140 / 1920);
        lvp.gravity = Gravity.CENTER;
        FrameLayout lineVertical = new FrameLayout(activity);
        lineVertical.setLayoutParams(lvp);
        lineVertical.setBackgroundColor(Color.rgb(209,209,209));

        followArea.addView(lineVertical);

        FrameLayout.LayoutParams ftsp = new FrameLayout.LayoutParams(width/2, height * 150 / 1920);
        ftsp.gravity = Gravity.RIGHT;
        followersText = new TextView(activity);
        followersText.setLayoutParams(ftsp);
        followersText.setGravity(Gravity.CENTER);
        followersText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8) * 0.46f);
        followersText.setTextColor(Color.BLACK);
        followersText.setText("Takipçi\n" + myFollowers.length());
        followersText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FollowFragment followFragment = new FollowFragment(activity,userInfo.getString("id"),"follower");
                    activity.addFragment(followFragment);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        followArea.addView(followersText);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width , height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        FrameLayout line = new FrameLayout(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(209,209,209));

        followArea.addView(line);
        container.addView(followArea);


        FrameLayout.LayoutParams livp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight + height * 630 / 1920));
        livp.topMargin = Utils.getBottom(followArea);
        listView = new ListView(activity);
        listView.setLayoutParams(livp);
        listView.setDividerHeight(0);
        adapter = new ProfileAdapter(activity);
        listView.setAdapter(adapter);
        listView.setDividerHeight(4);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        container.addView(listView);
    }

    public class ProfileAdapter extends ArrayAdapter<JSONObject> {

        private class ViewHolder {
            FrameLayout mainFrame,userInfoArea,line;
            TextView contentText, userNameText, timeText ,readArea;
            JSONObject jData;
            ImageView userLogo;
            FrameLayout.LayoutParams mfp, ctp;

            public ViewHolder(int position) {
                mfp = new FrameLayout.LayoutParams(width , elementSize);
                mfp.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
                mainFrame = new FrameLayout(getContext());
                mainFrame.setLayoutParams(mfp);
                mainFrame.setBackgroundColor(Color.WHITE);
                //mainFrame.setBackgroundResource(R.drawable.shadow_2749);

                FrameLayout.LayoutParams uiap = new FrameLayout.LayoutParams(width , logoWidth + height*20/1920);
                uiap.topMargin = height * 30 / 1920;
                userInfoArea = new FrameLayout(getContext());
                userInfoArea.setLayoutParams(uiap);
                userInfoArea.setBackgroundColor(Color.WHITE);

                FrameLayout.LayoutParams clp = new FrameLayout.LayoutParams(logoWidth, logoWidth);
                clp.leftMargin = width * 15 / 1080;
                userLogo = new ImageView(activity);
                userLogo.setLayoutParams(clp);
                userLogo.setBackgroundResource(R.drawable.profile_icon);

                userInfoArea.addView(userLogo);

                FrameLayout.LayoutParams untp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, logoWidth / 3);
                untp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                untp.topMargin = height * 15 / 1920;
                userNameText = new TextView(activity);
                userNameText.setLayoutParams(untp);
                userNameText.setPadding(0,height * 5 / 1920,0,0);
                userNameText.setGravity(Gravity.CENTER_VERTICAL);
                userNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.46f);
                userNameText.setTextColor(Color.BLACK);
                userNameText.setText("Abdullah Aydeniz");
                userNameText.setTypeface(regularFont);

                userInfoArea.addView(userNameText);

                FrameLayout.LayoutParams ttp = new FrameLayout.LayoutParams(width - logoWidth - width * 167 / 1080, logoWidth / 3);
                ttp.leftMargin = Utils.getRight(userLogo) + width * 48 / 1080;
                ttp.topMargin = Utils.getBottom(userNameText) + height * 5 / 1920;
                timeText = new TextView(activity);
                timeText.setLayoutParams(ttp);
                timeText.setPadding(0,height * 5 / 1920,0,0);
                timeText.setGravity(Gravity.CENTER_VERTICAL);
                timeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.46f);
                timeText.setTextColor(Color.rgb(144,144,144));
                timeText.setText("Dün 10:38'de");

                userInfoArea.addView(timeText);

                mainFrame.addView(userInfoArea);

                ctp = new FrameLayout.LayoutParams(width - width * 40 / 1080 , elementSize/2);
                ctp.topMargin = Utils.getBottom(userInfoArea);
                ctp.leftMargin = width * 20 / 1080;
                contentText = new TextView(getContext());
                contentText.setLayoutParams(ctp);
                contentText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(getContext()) * 5 / 8) * 0.50f);
                contentText.setGravity(Gravity.CENTER_VERTICAL);
                contentText.setTextColor(Color.BLACK);

                mainFrame.addView(contentText);


                FrameLayout.LayoutParams rap = new FrameLayout.LayoutParams(width , height * 145 / 1920);
                rap.gravity = Gravity.BOTTOM;
                rap.topMargin = Utils.getBottom(contentText);
                readArea = new TextView(getContext());
                readArea.setLayoutParams(rap);
                readArea.setBackgroundColor(Color.WHITE);
                readArea.setGravity(Gravity.CENTER);
                readArea.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(getContext()) * 6 / 8) * 0.50f);
                readArea.setTextColor(Color.rgb(73,157,255));
                readArea.setTypeface(mediumFont);
                readArea.setText("Oku");
                readArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            PostPartFragment postPartFragment = new PostPartFragment(activity,adapter.getItem(adapter.getPosition(jData)).getString("title"),adapter.getItem(adapter.getPosition(jData)).getString("root"),adapter.getItem(adapter.getPosition(jData)).getString("id"));
                            activity.addFragment(postPartFragment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                mainFrame.addView(readArea);

                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width , height * 4 / 1920);
                lp.gravity = Gravity.BOTTOM;
                lp.bottomMargin = Utils.getTop(readArea);
                line = new FrameLayout(getContext());
                line.setLayoutParams(lp);
                line.setBackgroundColor(Color.rgb(209,209,209));

                mainFrame.addView(line);
            }

            public void setData(JSONObject jData, View view) {
                this.jData = jData;
                try {
                    userNameText.setText(jData.getString("name"));
                    timeText.setText("@"+jData.getString("userName"));
                    contentText.setText(jData.getString("firstPart"));
                    int size = (int) contentText.getTextSize();
                    measureHeight = getHeight(getContext(), contentText.getText().toString(),size,width*686/750);
                    mfp.height = measureHeight+elementSize;
                    ctp.height = measureHeight;
                    //schoolName.setText(jData.getString("content"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        public ProfileAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ProfileAdapter.ViewHolder holder = null;
            if (convertView == null) {
                convertView = new FrameLayout(getContext());

                holder = new ProfileAdapter.ViewHolder(position);

                ((FrameLayout) convertView).addView(holder.mainFrame);
                /*AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize+height*40/1334);
                convertView.setLayoutParams(cp);*/
                convertView.setTag(holder);
            } else
                holder = (ProfileAdapter.ViewHolder) convertView.getTag();

            holder.setData(getItem(position),convertView);
            AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, measureHeight+elementSize);
            //cp.height = measureHeight + height*40/1334;
            convertView.setLayoutParams(cp);
            return convertView;
        }
    }


    @Override
    public void renderActionBar(ActionBar actionBar) {
        FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        /*FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        ImageView settingsIv = new ImageView(activity);
        settingsIv.setLayoutParams(bip);
        settingsIv.setImageResource(R.drawable.settings_black_icon);
        settingsIv.setPadding((actionHeight / 3), (actionHeight / 3), (actionHeight / 3), (actionHeight / 3));
        settingsIv.setScaleType(ImageView.ScaleType.FIT_XY);
        settingsIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        actionContainer.addView(settingsIv);
*/
        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        tp.leftMargin = /*Utils.getRight(settingsIv) + */width * 15 / 1080;
        TextView titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText("Profil");
        titleText.setTypeface(mediumfont);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        actionContainer.addView(titleText);

        FrameLayout.LayoutParams lop = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        lop.gravity = Gravity.RIGHT;
        ImageView logOut = new ImageView(activity);
        logOut.setLayoutParams(lop);
        logOut.setImageResource(R.drawable.log_out);
        logOut.setPadding((actionHeight / 5), (actionHeight / 5), (actionHeight / 5), (actionHeight / 5));
        logOut.setScaleType(ImageView.ScaleType.FIT_XY);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.signOut();
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
            }
        });
        actionContainer.addView(logOut);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);
    }

    public static int getHeight(Context context, String text, int textSize, int deviceWidth) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public void getPosts(){
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        try {
            remote.callAsync("POST", Constants.URL + Constants.URL_GET_POSTS_OF_PROFILE + "/" + userInfo.getString("id"), params, "getPosts", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addFollow(){
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();
        try {
            JSONObject user = new JSONObject(session.getString("user"));

            params.put("followedId",userInfo.getString("id"));
            params.put("followerId",user.getString("id"));
            params.put("followedName",userInfo.getString("name"));
            params.put("followerName",user.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        remote.callJsonAsync("POST", Constants.URL + Constants.URL_ADD_FOLLOWERS , params, "follow", false);
    }

    @Override
    public void responsed(String method, final JSONObject response) {
        if (method.equals("getPosts")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /*if (isFirst)
                        activity.dismisLoadingActivity();
                    else
                        swipeRefreshLayout.setRefreshing(false);*/
                    try {
                        adapter.clear();
                        JSONArray posts = response.getJSONArray("array");
                        for (int i = posts.length()-1 ; i >= 0 ; i--) {
                            adapter.add((JSONObject) posts.get(i));
                        }
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        } else if (method.equals("follow")) {
            try {
                response.getString("followedId");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (method.equals("getMyFolloweds")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        myFolloweds = response.getJSONArray("array");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        } else if (method.equals("getMyFollowers")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        myFollowers = response.getJSONArray("array");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    render();
                    getPosts();
                    //activity.dismisLoadingActivity();
                }
            });
        } else if (method.equals("remove")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Utils.showDialogMessage(activity, 1, "Anonimya", "Takipten çıktınız.");
                    //activity.dismisLoadingActivity();
                }
            });
        }
    }

    @Override
    public void failed(String method, JSONObject response) {

    }

    public GradientDrawable getGd() {
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(30);
        gd.setStroke(5, Color.rgb(26,41,51));
        if (isFollowed())
            gd.setColor(Color.YELLOW);
        else
            gd.setColor(Color.WHITE);
        return gd;
    }


    public void getMyFollowers() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        try {
            remote.callAsync("POST", Constants.URL + Constants.URL_GET_MY_FOLLOWERS + "/" + userInfo.getString("id"), params, "getMyFollowers", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getMyFolloweds() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        try {
            remote.callAsync("POST", Constants.URL + Constants.URL_GET_MY_FOLLOWEDS + "/" + userInfo.getString("id"), params, "getMyFolloweds", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removeFolloweds() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        try {
            remote.callAsync("POST", Constants.URL + Constants.URL_REMOVE_MY_FOLLOWEDS + "/" + activeFriendProfile.getString("id"), params, "remove", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isFollowed () {
        try {
            JSONObject user = new JSONObject(session.getString("user"));
            activeFriendProfile = new JSONObject();
            for (int i = 0 ; i < myFollowers.length() ; i++) {
                    if (myFollowers.getJSONObject(i).getString("followerId").equals(user.getString("id"))) {
                        activeFriendProfile = myFollowers.getJSONObject(i);
                        return true;
                    }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}

