package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class SharePostPartFragment extends BaseFragment implements Remotable,IActionBar{

    FrameLayout container;
    int width, height, sb, elementSize, logoWidth;
    int tabHeight, actionHeight, u, pad = 12;
    MainActivity activity;
    EditText part;
    Button btn;
    Boolean keyboardStatus = true;
    //Typeface mediumFont;
    String postId, parentId;
    Session session;
    JSONObject userInfo;

    public SharePostPartFragment(Context context, String postId, String parentId ) {
        this.postId = postId;
        this.parentId = parentId;
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        tabHeight = (int) Utils.getTabHeight(activity);
        u = (int) Utils.getUnitSize(activity);
        actionHeight = (int) Utils.getActionHeight(activity);
        elementSize = height * 170 / 1920;
        logoWidth = width * 120 / 1080;
        session = Session.getSession(activity);
        try {
            userInfo = new JSONObject(session.getString("user"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);
        showKeyboard();

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin=actionHeight;
        container.setLayoutParams(cp);
        container.setBackgroundColor(Color.rgb(26,41,51));


        render();
        created = true;

        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = container.getRootView().getHeight() - container.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");
                } else {
                    Log.e("MyActivity", "keyboard closed");
                }
            }
        });
    }

    public void render() {
        FrameLayout.LayoutParams pp = new FrameLayout.LayoutParams(width - 4 * pad, (int) u * 5 / 2);
        pp.leftMargin = 2 * pad;
        pp.topMargin =  2* pad;
        part = new EditText(activity);
        part.setGravity(Gravity.TOP | Gravity.LEFT);
        part.setPadding(pad, pad, 0, pad);
        part.setLayoutParams(pp);
        part.setHint("Part");
        part.setBackgroundColor(Color.rgb(255, 239, 219));
        part.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    part.setHint("");
                else
                    part.setHint("Part");
            }
        });

        container.addView(part);

        FrameLayout.LayoutParams bp = new FrameLayout.LayoutParams((int) u * 2, u);
        bp.gravity = Gravity.RIGHT;
        bp.rightMargin = 4 * pad;
        bp.topMargin = (int) u * 4 + 4 * pad;
        btn = new Button(activity);
        btn.setLayoutParams(bp);
        btn.setText("GÖNDER");
        btn.setBackgroundColor(Color.rgb(255, 239, 219));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPart();
            }
        });

        container.addView(btn);

    }


    public static int getHeight(Context context, String text, int textSize, int deviceWidth) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public void addPart(){
        activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();
        try {
            params.put("parent", parentId);
            params.put("postId", postId);
            params.put("status", "0");
            params.put("text", part.getText().toString());
            params.put("userId",userInfo.getString("id"));
            params.put("userName",userInfo.getString("userName"));
            params.put("name",userInfo.getString("name"));
            //params.put("device", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        remote.callJsonAsync("POST", Constants.URL + Constants.URL_ADD_PART, params, "addPart", false);
    }

    @Override
    public void responsed(String method, final JSONObject response) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.dismisLoadingActivity();
            }
        });
        if(method.equals("addPart")){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //((HomePageFragment)activity.getCurrentFragment()).updateAdapter(userInfo,"add");
                    /*HomePageRootFragment homePageRootFragment = new HomePageRootFragment();
                    homePageRootFragment.popFragment();
                    activity.getCurrentFragment();*/
                    //HomePageFragment.updateAdapter(userInfo,"add");
                }
            });
            Utils.showDialogMessageAndGoBack(activity,"","Part Ekleme başarılı");
        }
    }

    @Override
    public void failed(String method, JSONObject response) {

    }

    @Override
    public void renderActionBar(final ActionBar actionBar) {
        FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        bip.gravity = Gravity.CENTER_VERTICAL;
        ImageButton backIcon = new ImageButton(activity);
        backIcon.setImageResource(R.drawable.back_icon);
        backIcon.setLayoutParams(bip);
        backIcon.setPadding(actionHeight / 3, actionHeight / 3, actionHeight / 3, actionHeight / 3);
        backIcon.setScaleType(ImageView.ScaleType.FIT_XY);
        backIcon.setBackgroundColor(Color.WHITE);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });


        actionContainer.addView(backIcon);

        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        tp.leftMargin = /*Utils.getRight(settingsIv) + */width * 15 / 1080;
        TextView titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText("Paylaş");
        //titleText.setTypeface(mediumFont);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        actionContainer.addView(titleText);

        /*FrameLayout.LayoutParams cip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        cip.gravity = Gravity.RIGHT;
        ImageView carModeIv = new ImageView(activity);
        carModeIv.setLayoutParams(cip);
        carModeIv.setImageResource(R.drawable.car_mode_icon);
        carModeIv.setPadding((actionHeight / 4), (actionHeight / 4), (actionHeight / 4), (actionHeight / 4));
        carModeIv.setScaleType(ImageView.ScaleType.FIT_XY);
        carModeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.getPlayBar().isVisible()) {
                    CarModeFragment carModeFragment = new CarModeFragment(activity);
                    activity.addFragment(carModeFragment);
                }
            }
        });
        actionContainer.addView(carModeIv);*/

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = activity.getCurrentFocus();
        if (v != null)
            imm.showSoftInput(v, 0);
    }
}

