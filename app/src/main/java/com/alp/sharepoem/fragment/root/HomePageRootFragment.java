package com.alp.sharepoem.fragment.root;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.R;
import com.alp.sharepoem.fragment.CategoryFragment;
import com.alp.sharepoem.fragment.HomePageFragment;

import java.util.ArrayList;
import java.util.List;

public class HomePageRootFragment extends TabFragment {
    List<Fragment> stackFragment = new ArrayList<>();
    FrameLayout container;

    boolean created = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        if(!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = (FrameLayout) inflater.inflate(R.layout.fragment_home_page, cont,
                false);
        if(container.getParent()!=null)
            ((FrameLayout)container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (created)
            return;
        CategoryFragment categoryFragment = new CategoryFragment(getActivity());
        addFragment(categoryFragment);
        created = true;
    }

    public void addFragment(Fragment fragment){
        stackFragment.add(fragment);
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.home_page_root_frame, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    public void popFragment(){
        if(stackFragment.size() == 1){
            ((MainActivity)getActivity()).close();
            return;
        }
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.home_page_root_frame, stackFragment.get(stackFragment.size()-2));
        fragmentTransaction.commit();
        stackFragment.remove(stackFragment.size()-1);
    }

    @Override
    public void clearStackFragment(){
        created = false;
        Fragment fragment = null;
        if(stackFragment != null || stackFragment.size()>1)
            fragment = stackFragment.get(0);
        if(fragment != null){
            stackFragment.clear();
            addFragment(fragment);
        }
    }

    public void onCountryChange(){
        created = false;
        stackFragment.clear();
    }
}
