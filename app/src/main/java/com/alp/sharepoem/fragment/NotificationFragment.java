package com.alp.sharepoem.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;
import com.alp.sharepoem.view.EmptyView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class NotificationFragment extends Fragment implements IActionBar, Remotable {
    FrameLayout container, scrollFrame, languageFrame, recordFrame, countryFrame, myRecordsFrame, localFrame, popularFrame, lastPlayedFrame, sleepModeFrame;
    int width, height, sb, actionHeight, elementSize, tabHeight, logoSize, frameHeight;
    MainActivity activity;
    Session session;
    boolean created = false;
    Typeface mediumfont;
    NotificationsAdapter adapter;
    FrameLayout.LayoutParams lvp;
    ListView listView;
    JSONObject user;
    EmptyView emptyView;

    public NotificationFragment(Context context) {
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        actionHeight = (int) Utils.getActionHeight(activity);
        tabHeight = (int) Utils.getTabHeight(activity);
        elementSize = (height - actionHeight - tabHeight) / 6;
        logoSize = width * 140 / 1080;
        session = Session.getSession(activity);
        try {
            user = new JSONObject(session.getString("user"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created)
            container = new FrameLayout(getActivity());
        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);

        /*if (created) {
            Utils.setViewHeight(container, frameHeight);
            scrollFrame.setMinimumHeight((frameHeight>(elementSize * 8))?frameHeight:elementSize * 8);
            return;
        }*/

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin = actionHeight;
        container.setLayoutParams(cp);
        render();

        getMyNotifications();
    }

    public void render() {
        lvp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        listView = new ListView(activity);
        listView.setLayoutParams(lvp);
        listView.setDividerHeight(1);
        adapter = new NotificationsAdapter(activity);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        container.addView(listView);
    }


    public class NotificationsAdapter extends ArrayAdapter<JSONObject> {

        private class ViewHolder {
            FrameLayout mainFrame;
            ImageView rightArrow;
            TextView userNameText;
            JSONObject jNtfc;

            public ViewHolder() {
                FrameLayout.LayoutParams mfp = new FrameLayout.LayoutParams(width, elementSize);
                mainFrame = new FrameLayout(activity);
                mainFrame.setLayoutParams(mfp);
                mainFrame.setBackgroundColor(Color.WHITE);
                mainFrame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*ProfileFragment profileFragment = new ProfileFragment(activity,true,adapter.getItem(adapter.getPosition(jNtfc)));
                        activity.addFragment(profileFragment);*/
                    }
                });

                /*FrameLayout.LayoutParams clp = new FrameLayout.LayoutParams(logoWidth, logoWidth);
                clp.leftMargin = width * 48 / 1080;
                clp.gravity = Gravity.CENTER_VERTICAL;
                userLogo = new ImageView(activity);
                userLogo.setLayoutParams(clp);

                mainFrame.addView(userLogo);*/

                FrameLayout.LayoutParams ctp = new FrameLayout.LayoutParams(width  - width * 167 / 1080, elementSize);
                ctp.gravity = Gravity.CENTER_VERTICAL;
                ctp.leftMargin =  width * 48 / 1080;
                userNameText = new TextView(activity);
                userNameText.setLayoutParams(ctp);
                userNameText.setGravity(Gravity.CENTER_VERTICAL);
                userNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.48f);
                userNameText.setTextColor(Color.BLACK);
                mainFrame.addView(userNameText);

                FrameLayout.LayoutParams rap = new FrameLayout.LayoutParams(width * 40 / 1080, width * 40 / 1080);
                rap.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
                rap.rightMargin = width * 70 / 1080;
                rightArrow = new ImageView(activity);
                rightArrow.setBackgroundResource(R.drawable.right_arrow);
                rightArrow.setLayoutParams(rap);
                mainFrame.addView(rightArrow);
            }

            public void setData(JSONObject jNtfc) {
                this.jNtfc = jNtfc;
                try {
                    userNameText.setText(jNtfc.getString("notificationMessage"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        public NotificationsAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            AbsListView.LayoutParams cp = new AbsListView.LayoutParams(width, elementSize);
            if (convertView == null) {
                convertView = new FrameLayout(activity);
                convertView.setLayoutParams(cp);

                holder = new ViewHolder();

                ((FrameLayout) convertView).addView(holder.mainFrame);
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            holder.setData(getItem(position));
            return convertView;
        }
    }


    @Override
    public void renderActionBar(ActionBar actionBar) {
        FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        /*FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        ImageView settingsIv = new ImageView(activity);
        settingsIv.setLayoutParams(bip);
        settingsIv.setImageResource(R.drawable.settings_black_icon);
        settingsIv.setPadding((actionHeight / 3), (actionHeight / 3), (actionHeight / 3), (actionHeight / 3));
        settingsIv.setScaleType(ImageView.ScaleType.FIT_XY);
        settingsIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        actionContainer.addView(settingsIv);
*/
        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        tp.leftMargin = /*Utils.getRight(settingsIv) + */width * 15 / 1080;
        TextView titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText("Bildirimler");
        titleText.setTypeface(mediumfont);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        actionContainer.addView(titleText);

        /*FrameLayout.LayoutParams cip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        cip.gravity = Gravity.RIGHT;
        ImageView carModeIv = new ImageView(activity);
        carModeIv.setLayoutParams(cip);
        carModeIv.setImageResource(R.drawable.car_mode_icon);
        carModeIv.setPadding((actionHeight / 4), (actionHeight / 4), (actionHeight / 4), (actionHeight / 4));
        carModeIv.setScaleType(ImageView.ScaleType.FIT_XY);
        carModeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.getPlayBar().isVisible()) {
                    CarModeFragment carModeFragment = new CarModeFragment(activity);
                    activity.addFragment(carModeFragment);
                }
            }
        });
        actionContainer.addView(carModeIv);*/

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);
    }

    @Override
    public void responsed(String method, final JSONObject response) {
        if (method.equals("getMyNotifications")) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adapter.clear();
                        JSONArray notifications = response.getJSONArray("array");
                        for (int i = 0; i < notifications.length(); i++) {
                            adapter.add(notifications.getJSONObject(i));
                        }
                        adapter.notifyDataSetChanged();
                        /*if (adapter.getCount() == 0) {
                            emptyView = new EmptyView(activity, "search_empty_view",
                                    "Bildirimler", "Bildirim bulunmamaktadır.");
                            container.addView(emptyView);
                        }*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //activity.dismisLoadingActivity();
                }
            });
        }
    }

    @Override
    public void failed(String method, JSONObject response) {

    }

    public void getMyNotifications() {
        //activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        try {
            remote.callAsync("POST", Constants.URL + Constants.URL_GET_MY_NOTIFICATIONS + "/" + user.getString("id"), params, "getMyNotifications", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

