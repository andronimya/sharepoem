package com.alp.sharepoem.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;

public class MesFragment extends Fragment implements IActionBar {
    FrameLayout container, scrollFrame, languageFrame, recordFrame, countryFrame, myRecordsFrame, localFrame, popularFrame, lastPlayedFrame, sleepModeFrame;
    int width, height, sb, actionHeight, elementSize, tabHeight, logoSize, frameHeight;
    MainActivity activity;
    Session session;
    boolean created = false;
    Typeface mediumfont;

    public MesFragment(Context context) {
        activity = (MainActivity) context;
        sb = Utils.getStatusBarHeight(activity);
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity) - sb;
        actionHeight = (int) Utils.getActionHeight(activity);
        tabHeight = (int) Utils.getTabHeight(activity);
        elementSize = (height - actionHeight - tabHeight) / 9;
        logoSize = width * 140 / 1080;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup cont, Bundle savedInstanceState) {
        Utils.hideKeyboard(activity);
        if (!created)
            container = new ScrollView(getActivity());
        FrameLayout fRoot = new FrameLayout(getActivity());
        if (container.getParent() != null)
            ((FrameLayout) container.getParent()).removeView(container);
        fRoot.addView(container);
        return fRoot;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        renderActionBar(activity.getActionBr());
        activity.visibleTabBar(true);
        activity.setCurrentFragment(this);

        /*if (created) {
            Utils.setViewHeight(container, frameHeight);
            scrollFrame.setMinimumHeight((frameHeight>(elementSize * 8))?frameHeight:elementSize * 8);
            return;
        }*/
        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, height - (actionHeight + tabHeight));
        cp.topMargin = actionHeight;
        container.setLayoutParams(cp);
        container.setBackgroundColor(Color.YELLOW);
        render();
    }

    public void render() {

    }


    @Override
    public void renderActionBar(ActionBar actionBar) {
        FrameLayout actionContainer = actionBar;
        actionContainer.removeAllViews();
        actionContainer.setBackgroundColor(Color.WHITE);

        /*FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        ImageView settingsIv = new ImageView(activity);
        settingsIv.setLayoutParams(bip);
        settingsIv.setImageResource(R.drawable.settings_black_icon);
        settingsIv.setPadding((actionHeight / 3), (actionHeight / 3), (actionHeight / 3), (actionHeight / 3));
        settingsIv.setScaleType(ImageView.ScaleType.FIT_XY);
        settingsIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        actionContainer.addView(settingsIv);
*/
        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        tp.leftMargin = /*Utils.getRight(settingsIv) + */width * 15 / 1080;
        TextView titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText("Mesajlar");
        titleText.setTypeface(mediumfont);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        actionContainer.addView(titleText);

        /*FrameLayout.LayoutParams cip = new FrameLayout.LayoutParams(actionHeight, actionHeight);
        cip.gravity = Gravity.RIGHT;
        ImageView carModeIv = new ImageView(activity);
        carModeIv.setLayoutParams(cip);
        carModeIv.setImageResource(R.drawable.car_mode_icon);
        carModeIv.setPadding((actionHeight / 4), (actionHeight / 4), (actionHeight / 4), (actionHeight / 4));
        carModeIv.setScaleType(ImageView.ScaleType.FIT_XY);
        carModeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.getPlayBar().isVisible()) {
                    CarModeFragment carModeFragment = new CarModeFragment(activity);
                    activity.addFragment(carModeFragment);
                }
            }
        });
        actionContainer.addView(carModeIv);*/

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        actionContainer.addView(line);
    }
}

