package com.alp.sharepoem.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.LoginView;

import org.json.JSONObject;


public class LoginActivity extends Activity {

    FrameLayout container, topPart, userArea;
    int width, height, sb, actionHeight;
    float topPartHeight, userAreaHeight, userAreaTextHeight, space, lineTeacherWidth, userImageWidth, userImageTopMargin, userImageLeftMargin, userTextWidth, userTextLeftMargin, tickImageWidth;
    LoginView loginView;
    ImageView tickImage;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics dm = getResources().getDisplayMetrics();
        width = dm.widthPixels;
        height = dm.heightPixels;
        sb = Utils.getStatusBarHeight(this);
        actionHeight = (int) Utils.getActionHeight(this);
        topPartHeight = height * 522 / 1334;
        userAreaHeight = height-sb-topPartHeight;
        userAreaTextHeight = height * 110 / 1334;
        space = height * 3 / 1334;

        lineTeacherWidth = width * 10 / 750;
        userImageWidth = width * 80 / 750;
        userImageTopMargin = height * 62 / 1334;
        userImageLeftMargin = width * 40 / 750;
        userTextWidth = width * 590 / 750;
        userTextLeftMargin = width * 160 / 750;
        tickImageWidth = width * 180 / 750;
        session = Session.getSession(this);

        setTabBar();
        setContentView(container);
    }

    public void setTabBar() {
        FrameLayout.LayoutParams containerParams = new ScrollView.LayoutParams(width, height);
        container = new FrameLayout(this);
        container.setLayoutParams(containerParams);
        container.setBackgroundColor(Color.WHITE);
        container.setFocusable(true);
        container.setFocusableInTouchMode(true);

        /*FrameLayout.LayoutParams uap = new FrameLayout.LayoutParams(width, (int) userAreaHeight);
        uap.topMargin = Utils.getBottom(topPart);
        userArea = new FrameLayout(this);
        userArea.setBackgroundColor(Color.rgb(238, 238, 238));
        userArea.setLayoutParams(uap);*/

        loginView = new LoginView(this);
        container.addView(loginView);

        setContentView(container);
    }

    public void addIcon(int iconImage, FrameLayout layout) {
        FrameLayout.LayoutParams ip = new FrameLayout.LayoutParams(width * 100 / 750, width * 100 / 750);
        ip.gravity = Gravity.CENTER_VERTICAL;
        ip.leftMargin = (int) (actionHeight / 4);
        ImageView icon = new ImageView(this);
        icon.setLayoutParams(ip);
        icon.setImageResource(iconImage);
        layout.addView(icon);
    }

    public void showLoadingActivity(){
        Intent intent = new Intent(this, LoadingActivity.class);
        startActivity(intent);
    }

    public void dismisLoadingActivity(){
        LoadingActivity.getInstance().finish();
    }
}
