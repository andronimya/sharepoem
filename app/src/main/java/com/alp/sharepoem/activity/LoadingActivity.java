package com.alp.sharepoem.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Utils;
import com.wang.avi.AVLoadingIndicatorView;

public class LoadingActivity extends Activity {
    static LoadingActivity loadingActivity;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 26) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        session = new Session(getApplicationContext());
        boolean isChat = getIntent().getBooleanExtra("isChat",false);


        loadingActivity = this;
        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(Utils.getWidthPixels(this), Utils.getHeightPixels(this));
        FrameLayout container = new FrameLayout(this);
        container.setLayoutParams(cp);

        FrameLayout transparentScreen = new FrameLayout(this);
        transparentScreen.setLayoutParams(cp);
        if (!isChat) {
            transparentScreen.setBackgroundColor(Color.BLACK);
            transparentScreen.setAlpha((float) 0.7);
        }
        container.addView(transparentScreen);

        FrameLayout.LayoutParams gp = new FrameLayout.LayoutParams(Utils.getWidthPixels(this)/3, Utils.getWidthPixels(this)/3);
        gp.gravity = Gravity.CENTER;
        AVLoadingIndicatorView loadingView = new AVLoadingIndicatorView(this);
        loadingView.setLayoutParams(gp);
        loadingView.setIndicator("BallScaleMultipleIndicator");
        loadingView.setIndicatorColor(Color.rgb(35, 51, 80));
        container.addView(loadingView);

        setContentView(container);
    }
    public static LoadingActivity getInstance(){
        return loadingActivity;
    }

    @Override
    protected void onResume() {
        super.onResume();
        session.put("isOnline","true");
    }

    @Override
    protected void onPause() {
        super.onPause();
        session.put("isOnline","");
    }
}
