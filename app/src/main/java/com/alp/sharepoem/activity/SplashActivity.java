package com.alp.sharepoem.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;


import com.alp.sharepoem.R;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class SplashActivity extends Activity/* implements Remotable */{
    FrameLayout container;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = Session.getSession(this);

        /*session.put("deviceToken", FirebaseInstanceId.getInstance().getToken());
        if (!session.has("oldDeviceToken"))
            session.put("oldDeviceToken", FirebaseInstanceId.getInstance().getToken());*/

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        container = new FrameLayout(this);
        container.setLayoutParams(cp);
        container.setBackgroundColor(Color.rgb(35, 51, 80));

        /*cp = new FrameLayout.LayoutParams(Utils.getWidthPixels(this)/3, Utils.getWidthPixels(this)/3);
        cp.gravity = Gravity.CENTER;
        ImageView logo = new ImageView(this);
        logo.setLayoutParams(cp);
        logo.setImageResource(R.drawable.meis_logo);

        container.addView(logo);*/
        setContentView(container);

        Utils.getIsonDateFormat(new Date());

        /*if(Utils.isEmpty(session.getString("user"))) {
            try {
                JSONObject user = new JSONObject(session.getString("user"));
                Intent intent = new Intent(activity, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/


        if(Utils.isEmpty(session.getString("user"))){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }, 1000);

            return;
        } else {
            try {
                showLoadingActivity();
                JSONObject user = new JSONObject(session.getString("user"));
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        userStatus();
    }

    public void userStatus(){
        /*Remote remote=new Remote(this,this);
        JSONObject params=new JSONObject();
        /*int index = -1;
        if(getIntent().hasExtra("userId"));
            index = session.getUserAccountIndex(getIntent().getStringExtra("userId"));
        String token = "";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            String version = pInfo.versionName;
            params.put("appVersion",version);
            params.put("deviceType","ANDROID");
            params.put("deviceToken",session.getString("deviceToken"));
            JSONArray userAccounts = new JSONArray(session.getString("userAccounts"));
            if(index != -1 && index != 0)
                token = userAccounts.getJSONObject(index).getString("token");

            if(!session.getString("deviceToken").equals(session.getString("oldDeviceToken"))){
                params.put("oldDeviceToken", session.getString("oldDeviceToken"));
                JSONArray userIds = new JSONArray();
                for (int i = 0 ; i < userAccounts.length() ; i++ )
                    userIds.put(userAccounts.getJSONObject(i).getString("id"));
                params.put("userIds",userIds);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        remote.callJsonAsync("POST", Constants.URL + Constants.URL_SCHOOLS_USER_STATUS,
                params,
                "userStatus",
                false, token);*/

    }

    /*@Override
    public void responsed(String method, JSONObject response) {
        if(method.equals("userStatus")){
            session.put("oldDeviceToken",session.getString("deviceToken"));
            Session session = Session.getSession(this);
            session.createLoginSession(response);
            session.addUserAccounts(response);
            try {
                if(response.getString("userType").equals("shuttle")){
                    Intent intent= new Intent(this, MapActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Intent intent= new Intent(this, MainActivity.class);
            if(getIntent().hasExtra("notification")){
                intent.putExtra("notification", getIntent().getStringExtra("notification"));
                if (getIntent().hasExtra("jChat"))
                    intent.putExtra("jChat", getIntent().getStringExtra("jChat"));
                intent.putExtra("body", getIntent().getStringExtra("body"));
                intent.putExtra("title", getIntent().getStringExtra("title"));
                intent.putExtra("userId", getIntent().getStringExtra("userId"));
            }

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void failed(String method, JSONObject response) {
        Intent intent= new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }*/

    public void showLoadingActivity(){
        Intent intent = new Intent(this, LoadingActivity.class);
        startActivity(intent);
    }

    public void dismisLoadingActivity(){
        LoadingActivity.getInstance().finish();
    }
}
