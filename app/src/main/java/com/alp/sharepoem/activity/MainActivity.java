package com.alp.sharepoem.activity;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.alp.sharepoem.R;
import com.alp.sharepoem.fragment.NotificationFragment;
import com.alp.sharepoem.fragment.root.TabFragment;
import com.alp.sharepoem.utils.Utils;
import com.alp.sharepoem.view.ActionBar;
import com.alp.sharepoem.view.TabBar;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends FragmentActivity/* implements TimePickerFragment.PickTime*/ {
    ArrayList<Fragment> defaultFragments;
    ActionBar actionBar;
    int width, height;
    boolean doubleBackToExitPressedOnce = false;
    Fragment currentFragment;
    EditText searchEt;


    public TabFragment activeFragment;
    TabBar tabBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels - Utils.getStatusBarHeight(this);
        tabBar = new TabBar(this,getSupportFragmentManager());
        actionBar = new ActionBar(this);

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        FrameLayout container = new FrameLayout(this);
        container.setLayoutParams(cp);
        container.addView(tabBar);
        container.addView(actionBar);
        setContentView(container);

        onNewIntent(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        String notification = intent.getStringExtra("notification");
        if (!Utils.isEmpty(notification)) {
            //NotificationFragment notificationFragment = new NotificationFragment(MainActivity.this);
            tabBar.setCurrentTab(2);
            //addFragment(notificationFragment);
        }
        /*Intent sIntent = new Intent(this, SplashActivity.class);
        sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(sIntent);*/
    }

    @Override
    public void onBackPressed() {
        activeFragment.onBackPressed();
    }

    public void addFragment(Fragment fragment){
        activeFragment.addFragment(fragment);
    }



    public void close(){
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Uygulamadan çıkmak için tekrar geriye basın.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public ActionBar getActionBr() {
        return actionBar;
    }
    public TabBar getTabBar(){
        return tabBar;
    }

    public void visibleTabBar(boolean isVisible) {
        tabBar.visibleTabBar(isVisible);
    }

    public void setCurrentFragment(Fragment fragment) {
        this.currentFragment = fragment;
    }

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    public void startVoiceInput(EditText searchEt) {
        this.searchEt = searchEt;
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hello, How can I help you?");
        try {
            startActivityForResult(intent, 1);
        } catch (ActivityNotFoundException a) {

        }
    }

    public void showLoadingActivity(){
        Intent intent = new Intent(this, LoadingActivity.class);
        startActivity(intent);
    }

    public void dismisLoadingActivity(){
        LoadingActivity.getInstance().finish();
    }


}
