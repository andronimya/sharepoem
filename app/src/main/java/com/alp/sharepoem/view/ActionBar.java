package com.alp.sharepoem.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.utils.Utils;

public class ActionBar extends FrameLayout {
    TextView radioName;
    int width, height, sb;
    float actionHeight;
    Typeface font;
    MainActivity activity;
    ImageView searchIcon;
    TextView titleText;


    public ActionBar(Context context) {
        super(context);
        activity = (MainActivity) context;
        render();
    }

    public void render() {
        this.removeAllViews();
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity);
        actionHeight = (int) Utils.getActionHeight(activity);
        sb = Utils.getStatusBarHeight(activity);

        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(width, (int) actionHeight);
        cp.gravity = Gravity.TOP;
        setLayoutParams(cp);
        setBackgroundColor(Color.WHITE);

        removeAllViews();

        FrameLayout.LayoutParams tp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tp.gravity = Gravity.CENTER;
        //tp.leftMargin = width * 50 / 1080;
        titleText = new TextView(activity);
        titleText.setLayoutParams(tp);
        titleText.setText("Kategoriler");
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.53f));
        titleText.setTextColor(Color.BLACK);
        addView(titleText);

        /*FrameLayout.LayoutParams sip = new FrameLayout.LayoutParams((int)actionHeight,(int) actionHeight);
        sip.gravity = Gravity.RIGHT;
        searchIcon = new ImageView(activity);
        searchIcon.setLayoutParams(sip);
        searchIcon.setImageResource(R.drawable.search_icon);
        searchIcon.setPadding((int)(actionHeight / 3), (int)(actionHeight / 3), (int)(actionHeight / 3), (int)(actionHeight / 3));
        searchIcon.setScaleType(ImageView.ScaleType.FIT_XY);
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                FrameLayout.LayoutParams sfp = new FrameLayout.LayoutParams(width-width*20/1080, (int) (actionHeight-height*20/1920));
                sfp.gravity = Gravity.CENTER;
                final FrameLayout searchFrame = new FrameLayout(activity);
                searchFrame.setLayoutParams(sfp);
                searchFrame.setBackgroundColor(Color.WHITE);

                GradientDrawable gd = new GradientDrawable();
                gd.setShape(GradientDrawable.RECTANGLE);
                gd.setColor(Color.WHITE);
                gd.setStroke(4, Color.rgb(211, 211, 211));
                searchFrame.setBackground(gd);

                FrameLayout.LayoutParams bip = new FrameLayout.LayoutParams( (int)(5*actionHeight/6), (int)(5*actionHeight/6) );
                bip.gravity = Gravity.CENTER_VERTICAL;
                bip.leftMargin = width*50/1080;
                ImageButton backIcon = new ImageButton(activity);
                backIcon.setImageResource(R.drawable.search_back);
                backIcon.setLayoutParams(bip);
                backIcon.setPadding((int)(actionHeight )/4 ,(int) (actionHeight) / 4,  (int)(actionHeight) / 4, (int) (actionHeight / 4));
                backIcon.setScaleType(ImageView.ScaleType.FIT_XY);
                backIcon.setBackgroundColor(Color.WHITE);
                backIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removeView(searchFrame);
                        Utils.hideKeyboard(activity);
                    }
                });

                searchFrame.addView(backIcon);

                FrameLayout.LayoutParams etp = new FrameLayout.LayoutParams((int)(width-2*actionHeight-width*100/1080), (int)(actionHeight));
                etp.gravity = Gravity.CENTER_VERTICAL;
                etp.leftMargin = Utils.getRight(backIcon);
                EditText searchEt = new EditText(activity);
                searchEt.setLayoutParams(etp);
                searchEt.setHint("Ara");
                searchEt.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8 * 0.48f));
                searchEt.setTextColor(Color.GRAY);

                searchFrame.addView(searchEt);
                addView(searchFrame);
            }
        });
        addView(searchIcon);*/

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height * 5 / 1920);
        lp.gravity = Gravity.BOTTOM;
        ImageView line = new ImageView(activity);
        line.setLayoutParams(lp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        addView(line);

    }
    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(android.content.Context.INPUT_METHOD_SERVICE);

        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus()
                        .getWindowToken(), 0);
    } // hideKeyboard
}
