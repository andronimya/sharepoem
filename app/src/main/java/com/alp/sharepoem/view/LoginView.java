package com.alp.sharepoem.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.LoginActivity;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.azimolabs.maskformatter.MaskFormatter;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginView extends FrameLayout implements Remotable {
    FrameLayout container, passwordFrame, userNameFrame, topImageFrame;
    TextView  loginButton, userNameHeaderText, passwordHeaderText, passwordForgottenText,signUpText;
    EditText userNameEdit, passwordEdit;
    ImageView topImage, logo;
    Button refreshButton;
    int width, height, sb;
    float tabHeight, actionHeight, u;
    LoginActivity activity;
    Typeface fontRegular, fontBold;
    MaskFormatter maskFormatter;
    Session session;
    int softWareKeysHeight,previousHeightDiffrence;
    boolean isNavBar,isAdd =false;
    JSONArray userAccounts = new JSONArray();
    JSONObject userAccount = new JSONObject();
    JSONArray newUserAccounts = new JSONArray();

    ArrayList<ImageView> lineArray = new ArrayList<>() ;
    int editTextHightSize,textHightSize, textWidthSize, arrowLeftMargin, textHeaderTopMargin, elementFrameSizeHeight,elementFrameLeftMargin, elementFrameSizeWidth, elementFrameTopMargin;


    public LoginView(Context context) {
        super(context);
        activity = (LoginActivity) context;
        fontRegular = Utils.getRegularFont(activity);
        fontBold = Utils.getBoldFont(activity);
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels - Utils.getStatusBarHeight(getContext());
        sb = Utils.getStatusBarHeight(activity);
        u = Utils.getUnitSize(activity);
        actionHeight = Utils.getActionHeight(activity);
        softWareKeysHeight = Utils.getNavigationBarHeight(activity);
        isNavBar = Utils.hasNavBar();
        render();

    }

    public void render() {

        textHightSize = height * 48 / 1334;
        textWidthSize = width * 260 / 750;
        arrowLeftMargin = width * 260 / 750;
        textHeaderTopMargin = height * 25 / 1334;
        elementFrameSizeHeight = height * 133 / 1334;
        elementFrameLeftMargin = width * 40 / 750;
        elementFrameSizeWidth = width / 2 - width * 60 / 750;
        elementFrameTopMargin = height * 350 / 1334;
        editTextHightSize = height * 90 / 1334;



        LayoutParams cp = new LayoutParams(width, height);
        setLayoutParams(cp);
        container = new FrameLayout(activity);
        container.setLayoutParams(cp);
        container.setFocusableInTouchMode(true);
        container.requestFocus();

        /*LayoutParams tifp = new LayoutParams(width, height * 382 / 1334);
        tifp.gravity = Gravity.TOP;
        topImageFrame = new FrameLayout(activity);
        topImageFrame.setLayoutParams(tifp);

        LayoutParams tip = new LayoutParams(width, height * 382 / 1334);
        topImage = new ImageView(activity);
        topImage.setLayoutParams(tip);
        //topImage.setBackgroundResource(R.drawable.top_part_image);
        topImageFrame.addView(topImage);

        LayoutParams lp = new LayoutParams(width * 140 / 750, width * 140 / 750);
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.topMargin=height*60/1334;
        logo = new ImageView(activity);
        logo.setLayoutParams(lp);
        logo.setBackgroundResource(R.drawable.meis_logo);
        topImageFrame.addView(logo);

        container.addView(topImageFrame);

        lp = new LayoutParams((int) (actionHeight), (int) (actionHeight));
        lp.leftMargin = (int) (actionHeight / 4);
        lp.topMargin = (int) (actionHeight / 4);
        ImageView backIcon = new ImageView(activity);
        backIcon.setLayoutParams(lp);
        backIcon.setPadding((int) (actionHeight/4),(int) (actionHeight/4),(int) (actionHeight/4),(int) (actionHeight/4));
        backIcon.setImageResource(R.drawable.back_icon);
        backIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });

        container.addView(backIcon);*/


        LayoutParams unfp = new LayoutParams(elementFrameSizeWidth * 2, elementFrameSizeHeight);
        unfp.gravity = Gravity.CENTER_HORIZONTAL;
        unfp.topMargin =  elementFrameTopMargin;
        userNameFrame = new FrameLayout(activity);
        userNameFrame.setLayoutParams(unfp);


        LayoutParams unht = new LayoutParams(textWidthSize * 2, textHightSize);
        unht.gravity = Gravity.CENTER_HORIZONTAL;
        unht.topMargin = textHightSize + textHeaderTopMargin;
        userNameHeaderText = new TextView(activity);
        userNameHeaderText.setLayoutParams(unht);
        userNameHeaderText.setTextColor(Color.BLACK);
        userNameHeaderText.setText("Kullanıcı Adı");
        userNameHeaderText.setTypeface(fontBold);
        userNameHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        userNameHeaderText.setGravity(Gravity.CENTER);
        userNameFrame.addView(userNameHeaderText);

        LayoutParams unep = new LayoutParams(textWidthSize * 2, editTextHightSize);
        unep.gravity = Gravity.CENTER_HORIZONTAL;
        unep.topMargin =  textHightSize + textHeaderTopMargin;
        userNameEdit = new EditText(activity);
        userNameEdit.setLayoutParams(unep);
        userNameEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        userNameEdit.setBackgroundColor(Color.TRANSPARENT);
        userNameEdit.setTextColor(Color.BLACK);
        userNameEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        userNameEdit.setTypeface(fontRegular);
        userNameEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        userNameEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        userNameEdit.setText("alper");
        userNameEdit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                changeParams(0, userNameHeaderText, userNameEdit);
                return false;
            }
        });
        userNameFrame.addView(userNameEdit);

        container.addView(userNameFrame);
        setLine(userNameFrame, elementFrameSizeWidth * 2 + elementFrameLeftMargin * 2);


        unfp = new LayoutParams(elementFrameSizeWidth * 2, elementFrameSizeHeight);
        unfp.gravity = Gravity.CENTER_HORIZONTAL;
        unfp.topMargin = Utils.getBottom(userNameFrame) + height * 80/1334;
        passwordFrame = new FrameLayout(activity);
        passwordFrame.setLayoutParams(unfp);

        unht = new LayoutParams(textWidthSize * 2, textHightSize);
        unht.gravity = Gravity.CENTER_HORIZONTAL;
        unht.topMargin = textHightSize + textHeaderTopMargin;
        passwordHeaderText = new TextView(activity);
        passwordHeaderText.setLayoutParams(unht);
        passwordHeaderText.setTextColor(Color.BLACK);
        passwordHeaderText.setText("Şifre");
        passwordHeaderText.setTypeface(fontBold);
        passwordHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        passwordHeaderText.setGravity(Gravity.CENTER);
        passwordFrame.addView(passwordHeaderText);

        unep = new LayoutParams(textWidthSize * 2, editTextHightSize);
        unep.gravity = Gravity.CENTER_HORIZONTAL;
        unep.topMargin =  textHightSize + textHeaderTopMargin;
        passwordEdit = new EditText(activity);
        passwordEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        passwordEdit.setLayoutParams(unep);
        passwordEdit.setTextColor(Color.BLACK);
        passwordEdit.setBackgroundColor(Color.TRANSPARENT);
        passwordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordEdit.setTypeface(fontRegular);
        passwordEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        passwordEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        passwordEdit.setText("123456");
        passwordEdit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                changeParams(1, passwordHeaderText, passwordEdit);
                return false;
            }
        });
        passwordFrame.addView(passwordEdit);

        container.addView(passwordFrame);
        setLine(passwordFrame, elementFrameSizeWidth * 2 + elementFrameLeftMargin * 2);


        LayoutParams lbp = new LayoutParams(elementFrameSizeWidth * 2, height * 120 / 1334);
        lbp.gravity = Gravity.CENTER_HORIZONTAL;
        lbp.topMargin = Utils.getBottom(passwordFrame) + height*80/1334 ;
        loginButton = new TextView(activity);
        loginButton.setLayoutParams(lbp);
        loginButton.setText("Giriş Yap");
        loginButton.setTypeface(fontRegular);
        loginButton.setBackgroundResource(R.drawable.shadow_button);
        loginButton.setGravity(Gravity.CENTER);
        // loginButton.setBackgroundColor(Color.RED);
        loginButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.70f);
        loginButton.setTextColor(Color.WHITE);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isEmpty(userNameEdit.getText().toString()) && !Utils.isEmpty(passwordEdit.getText().toString())) {
                    login();
                } else {
                    if (Utils.isEmpty(userNameEdit.getText().toString()))
                        Utils.showDialogMessage(activity, 0, "Uyarı", "Lütfen telefon numaranızı giriniz.");
                    else
                        Utils.showDialogMessage(activity, 0, "Uyarı", "Lütfen şifrenizi giriniz.");
                }

            }
        });
        container.addView(loginButton);



        LayoutParams pfbp = new LayoutParams(width * 250 / 750, textHightSize);
        pfbp.gravity= Gravity.BOTTOM;
        pfbp.leftMargin = width * 30 / 750;
        pfbp.bottomMargin = width * 25 / 750;
        passwordForgottenText = new TextView(activity);
        passwordForgottenText.setLayoutParams(pfbp);
        passwordForgottenText.setText("Şifremi Unuttum");
        passwordForgottenText.setTypeface(fontBold);
        passwordForgottenText.setGravity(Gravity.LEFT);
        passwordForgottenText.setTextColor(Color.BLACK);
        passwordForgottenText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setTitle("      Şifrenizi mi unuttunuz?");
                final EditText phoneEdit = new EditText(activity);
                maskFormatter = new MaskFormatter("9999 999 99 99", phoneEdit);
                phoneEdit.setHint("Telefon Numaranızı Giriniz");
                phoneEdit.setGravity(Gravity.CENTER_HORIZONTAL);
                phoneEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
                phoneEdit.addTextChangedListener(maskFormatter);
                phoneEdit.setMinimumHeight((int) (actionHeight));
                LinearLayout layout = new LinearLayout(activity);
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.addView(phoneEdit);
                alert.setView(layout);

                alert.setPositiveButton("Gönder", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getSMS(maskFormatter.getRawTextValue().toString());
                    }
                });

                alert.setNegativeButton("İptal Et", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });

                alert.show();
            }
        });
        container.addView(passwordForgottenText);


        LayoutParams sup = new LayoutParams(width * 250 / 750, textHightSize);
        sup.gravity= Gravity.RIGHT |Gravity.BOTTOM;
        sup.rightMargin = width * 30 / 750;
        sup.bottomMargin = width * 25 / 750;
        signUpText = new TextView(activity);
        signUpText.setLayoutParams(sup);
        signUpText.setText("Kayıt Ol!");
        signUpText.setTypeface(fontBold);
        signUpText.setGravity(Gravity.RIGHT);
        signUpText.setTextColor(Color.BLACK);
        signUpText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (Utils.isEmpty(maskFormatter.getRawTextValue().toString()))
                    Utils.showDialogMessage(activity, 0, "Meis", "Lütfen telefon numarası giriniz.");
                else {
                    if (!Utils.checkPhone(maskFormatter.getRawTextValue().toString()))
                        Utils.showDialogMessage(activity, 0, "Meis", "Lütfen geçerli bir telefon numarası giriniz.");
                    else
                        getSMS();
                }*/
                SignUpView signUpView = new SignUpView(activity);
                activity.setContentView(signUpView);
                //container.addView(signUpView);
            }
        });
        container.addView(signUpText);

        addView(container);


        if (!isNavBar)
            softWareKeysHeight = 0;
        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                container.getWindowVisibleDisplayFrame(r);
                int keyboardHeight = height - (r.bottom);
                if (previousHeightDiffrence != keyboardHeight) {
                    previousHeightDiffrence = keyboardHeight;
                    if (keyboardHeight > 100) {
                        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
                        params.topMargin = -(height + softWareKeysHeight - r.bottom - sb) / 2;
                        container.setLayoutParams(params);
                    } else {
                        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
                        params.topMargin = 0;
                        container.setLayoutParams(params);
                    }
                }
            }
        });
    }



    public void changeParams(int lineIndex, View headerText, View subText){
        for(ImageView line : lineArray)
            line.setBackgroundColor(Color.rgb(238, 238, 238));
        lineArray.get(lineIndex).setBackgroundColor(Color.rgb(238, 238, 238));

        LayoutParams textHeaderParams = new LayoutParams(textWidthSize, textHightSize);
        textHeaderParams.gravity = Gravity.CENTER_HORIZONTAL;
        textHeaderParams.topMargin = textHeaderTopMargin;
        headerText.setLayoutParams(textHeaderParams);

        if( subText instanceof EditText) {
            LayoutParams textParams = new LayoutParams(textWidthSize * 2, editTextHightSize);
            textParams.gravity = Gravity.CENTER_HORIZONTAL;
            textParams.topMargin = Utils.getBottom(headerText) - (height * 15 / 1334);
            subText.setLayoutParams(textParams);
        }
    }

    public void setLine(FrameLayout frame, int lineWidth) {
        ImageView line = new ImageView(activity);
        LayoutParams vp = new LayoutParams(lineWidth, height * 3 / 1334);
        vp.gravity = Gravity.BOTTOM  | Gravity.CENTER_HORIZONTAL;
        line.setLayoutParams(vp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        frame.addView(line);
        lineArray.add(line);
    }

    public void getSMS(String phoneNumber) {
        activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();

        remote.callAsync("POST",Constants.URL +Constants.URL_USERS_SEND_PASSWORD + "/" +
                        phoneNumber,
                        params,
                        "sendPassword",
                        false);
    }

    public void login() {
        activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();
        try {
            params.put("userName", userNameEdit.getText().toString());
            params.put("password", passwordEdit.getText().toString());
            params.put("deviceToken", FirebaseInstanceId.getInstance().getToken());

            //params.put("device", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        remote.callAsync("POST", Constants.URL + Constants.URL_USERS_LOGIN, params, "login", false);
    }

    @Override
    public void responsed(String method, JSONObject response) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.dismisLoadingActivity();
            }
        });
        if (method.equals("login")) {
            session = Session.getSession(activity);
            session.put("user",response.toString());
            Intent intent = new Intent(activity, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
        }else if(method.equals("sendPassword")){
            try {
                Utils.showDialogMessage(activity,0,"",response.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void failed(String method, JSONObject response) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.dismisLoadingActivity();
            }
        });
        try {
            Utils.showDialogMessage(activity,0,"",response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}