package com.alp.sharepoem.view;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.utils.Utils;

public class EmptyView extends FrameLayout{



    ImageView emptyViewImage;
    int width, height, sb;
    float tabHeight, actionHeight, u, size;
    MainActivity activity;
    Typeface fontRegular;
    String imageName,alert,solutionAlert;
    TextView solutionAlertText,alertText;
    Context context;

    public EmptyView(Context context, String imageName, String alert, String solutionAlert) {
        super(context);
        activity = (MainActivity) context;
        fontRegular = Utils.getRegularFont(activity);
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels - Utils.getStatusBarHeight(getContext());
        sb = Utils.getStatusBarHeight(activity);
        tabHeight = Utils.getTabHeight(activity);
        u = Utils.getUnitSize(activity);
        actionHeight = Utils.getActionHeight(activity);
        this.imageName = imageName;
        this.context = context;
        this.alert = alert;
        this.solutionAlert = solutionAlert;
        render();
    }

    public void render() {
        LayoutParams cp = new LayoutParams(width, (int) (height - actionHeight));
        if (imageName.equals("school_bell_empty_image") || imageName.equals("attendance_empty_image"))
          cp =new LayoutParams(width, (int) (height - (actionHeight + Utils.getTabHeight(activity))));
        //cp.topMargin = (int) (actionHeight)+height*3/1334;
        setLayoutParams(cp);
        setBackgroundColor(Color.WHITE);

        LayoutParams evip = new LayoutParams(width*426/750, width*426/750);
        evip.topMargin = height*90/1334;
        evip.gravity = Gravity.CENTER_HORIZONTAL;
        emptyViewImage = new ImageView(activity);
        emptyViewImage.setLayoutParams(evip);
        int iconResId = getResources().getIdentifier(imageName, "drawable",context.getPackageName());
        emptyViewImage.setImageResource(iconResId);
        addView(emptyViewImage);

        LayoutParams atp = new LayoutParams(width, height*60/1334);
        atp.topMargin = Utils.getBottom(emptyViewImage)+ height*80/1334;
        atp.gravity = Gravity.CENTER_HORIZONTAL;
        alertText = new TextView(activity);
        alertText.setLayoutParams(atp);
        alertText.setTextColor(Color.rgb(94,94,94));
        alertText.setGravity(Gravity.CENTER_HORIZONTAL);
        alertText.setText(alert);
        alertText.setTypeface(fontRegular);
        alertText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 6 / 8) * 0.48f);
        addView(alertText);

        LayoutParams satp = new LayoutParams(width-width*100/750, height*120/1334);
        satp.topMargin = Utils.getBottom(alertText)+ height*60/1334;
        satp.gravity = Gravity.CENTER_HORIZONTAL;
        solutionAlertText = new TextView(activity);
        solutionAlertText.setLayoutParams(satp);
        solutionAlertText.setTextColor(Color.rgb(155,155,155));
        solutionAlertText.setGravity(Gravity.CENTER_HORIZONTAL);
        solutionAlertText.setText(solutionAlert);
        solutionAlertText.setTypeface(fontRegular);
        solutionAlertText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.48f);
        addView(solutionAlertText);
    }

    public void changeText(String alert, String solutionAlert){
        this.alert = alert;
        this.solutionAlert = solutionAlert;
        alertText.setText(alert);
        solutionAlertText.setText(solutionAlert);
    }

    public void setLayoutParams(int width, int height,int topMargin){
        LayoutParams cp = new LayoutParams(width, height);
        cp.topMargin = topMargin;
        setLayoutParams(cp);
    }
}