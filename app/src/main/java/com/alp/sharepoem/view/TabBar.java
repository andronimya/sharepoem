package com.alp.sharepoem.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabWidget;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.fragment.root.MessageRootFragment;
import com.alp.sharepoem.fragment.root.NotificationRootFragment;
import com.alp.sharepoem.fragment.root.HomePageRootFragment;
import com.alp.sharepoem.fragment.root.ProfileRootFragment;
import com.alp.sharepoem.fragment.root.SearchRootFragment;
import com.alp.sharepoem.fragment.root.TabFragment;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;

public class TabBar extends FragmentTabHost {
    MainActivity activity;
    FragmentManager fragmentManager;
    TabWidget tabBar;
    FrameLayout homePageFrame, searchFrame, messageFrame, notificationFrame, profileFrame;
    ImageView homePageImage, searchImage, messageImage, notificationImage, profileImage;
    TextView categoriesText, myFavoriesText, searchText, browseText;
    int width, height, imageSize, textSize, iconTopMargin, textTopMargin;
    int tabHeight, elementWitdh;
    Typeface font;
    FrameLayout.LayoutParams mfp, sfp, msfp, nfp, pfp;
    int currentAction = 0;
    ActionBar actionBar;

    public TabBar(Context context, FragmentManager fragmentManager) {
        super(context);
        activity = (MainActivity) context;
        this.fragmentManager = fragmentManager;
        width = Utils.getWidthPixels(activity);
        height = Utils.getHeightPixels(activity);
        tabHeight = (int)Utils.getTabHeight(activity);
        elementWitdh = width / 4;
        imageSize = width * 80 / 1080;
        textSize = width * 54 / 1080;
        iconTopMargin = width * 44 / 1080;
        textTopMargin = width * 115 / 1080;
        render();
    }

    private void render() {
        FrameLayout.LayoutParams cp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        setLayoutParams(cp);
        setId(android.R.id.tabhost);

        cp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        FrameLayout frame = new FrameLayout(activity);
        frame.setLayoutParams(cp);

        cp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        FrameLayout tabContent = new FrameLayout(activity);
        tabContent.setLayoutParams(cp);
        tabContent.setId(android.R.id.tabcontent);

        cp = new FrameLayout.LayoutParams(width, (int)tabHeight);
        cp.gravity = Gravity.BOTTOM;
        tabBar = new TabWidget(activity);
        tabBar.setLayoutParams(cp);
        tabBar.setId(android.R.id.tabs);
        tabBar.setBackgroundColor(Color.rgb(230, 230, 230));
        tabBar.setPadding(0,width * 4 / 1080, 0, 0);

        frame.addView(tabContent);
        frame.addView(tabBar);
        addView(frame);

        setup(activity, fragmentManager, android.R.id.tabcontent);

        mfp = new FrameLayout.LayoutParams((int) elementWitdh, (int) tabHeight);
        mfp.gravity = Gravity.CENTER;
        homePageFrame = new FrameLayout(activity);
        homePageFrame.setLayoutParams(mfp);
        homePageFrame.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams mip = new FrameLayout.LayoutParams(imageSize, imageSize);
        //mip.topMargin = iconTopMargin;
        mip.gravity = Gravity.CENTER;
        homePageImage = new ImageView(activity);
        homePageImage.setLayoutParams(mip);
        homePageFrame.addView(homePageImage);

        /*FrameLayout.LayoutParams rctp = new FrameLayout.LayoutParams((int) elementWitdh, textSize);
        rctp.topMargin = textTopMargin;
        rctp.gravity = Gravity.CENTER_HORIZONTAL;
        categoriesText = new TextView(activity);
        categoriesText.setGravity(Gravity.CENTER_HORIZONTAL);
        categoriesText.setLayoutParams(rctp);
        categoriesText.setText("Kategoriler");
        categoriesText.setPadding(0, 0, 0, 0);
        categoriesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.465f);
        categoriesText.setTypeface(font, Typeface.BOLD);
        homePageFrame.addView(categoriesText);*/

        sfp = new FrameLayout.LayoutParams((int) elementWitdh, (int) tabHeight);
        //sfp.leftMargin = Utils.getRight(homePageFrame);
        sfp.gravity = Gravity.CENTER;
        searchFrame = new FrameLayout(activity);
        searchFrame.setLayoutParams(sfp);
        searchFrame.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams sip = new FrameLayout.LayoutParams(imageSize, imageSize);
        //sip.topMargin = iconTopMargin;
        sip.gravity = Gravity.CENTER;
        searchImage = new ImageView(activity);
        searchImage.setLayoutParams(sip);
        searchFrame.addView(searchImage);

        /*FrameLayout.LayoutParams mftp = new FrameLayout.LayoutParams((int) elementWitdh, textSize);
        mftp.topMargin = textTopMargin;
        mftp.gravity = Gravity.CENTER_HORIZONTAL;
        myFavoriesText = new TextView(activity);
        myFavoriesText.setGravity(Gravity.CENTER_HORIZONTAL);
        myFavoriesText.setLayoutParams(mftp);
        myFavoriesText.setText("Favorilerim");
        myFavoriesText.setPadding(0, 0, 0, 0);
        myFavoriesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.465f);
        myFavoriesText.setTypeface(font, Typeface.BOLD);
        searchFrame.addView(myFavoriesText);*/

        /*msfp = new FrameLayout.LayoutParams((int) elementWitdh, (int) tabHeight);
        //msfp.leftMargin = Utils.getRight(searchFrame);
        msfp.gravity = Gravity.CENTER;
        messageFrame = new FrameLayout(activity);
        messageFrame.setLayoutParams(msfp);
        messageFrame.setBackgroundColor(Color.WHITE);


        FrameLayout.LayoutParams msip = new FrameLayout.LayoutParams(imageSize, imageSize);
        //msip.topMargin = iconTopMargin;
        msip.gravity = Gravity.CENTER;
        messageImage = new ImageView(activity);
        messageImage.setLayoutParams(msip);
        messageFrame.addView(messageImage);*/

        /*FrameLayout.LayoutParams rtp = new FrameLayout.LayoutParams((int) elementWitdh, textSize);
        rtp.topMargin = textTopMargin;
        rtp.gravity = Gravity.CENTER_HORIZONTAL;
        searchText = new TextView(activity);
        searchText.setGravity(Gravity.CENTER_HORIZONTAL);
        searchText.setLayoutParams(rtp);
        searchText.setText("Arama");
        searchText.setPadding(0, 0, 0, 0);
        searchText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.465f);
        searchText.setTypeface(font, Typeface.BOLD);
        messageFrame.addView(searchText);*/

        nfp = new FrameLayout.LayoutParams((int) elementWitdh, (int) tabHeight);
        //nfp.leftMargin = Utils.getRight(messageFrame);
        nfp.gravity = Gravity.CENTER;
        notificationFrame = new FrameLayout(activity);
        notificationFrame.setLayoutParams(nfp);
        notificationFrame.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams nip = new FrameLayout.LayoutParams(imageSize, imageSize);
        //nip.topMargin = iconTopMargin;
        nip.gravity = Gravity.CENTER;
        notificationImage = new ImageView(activity);
        notificationImage.setLayoutParams(nip);
        notificationFrame.addView(notificationImage);

        /*FrameLayout.LayoutParams btp = new FrameLayout.LayoutParams(width * 360 / 1080, textSize);
        btp.topMargin = textTopMargin;
        btp.gravity = Gravity.CENTER_HORIZONTAL;
        browseText = new TextView(activity);
        browseText.setGravity(Gravity.CENTER_HORIZONTAL);
        browseText.setLayoutParams(btp);
        browseText.setText("Gözat");
        browseText.setPadding(0, 0, 0, 0);
        browseText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.465f);
        browseText.setTypeface(font, Typeface.BOLD);
        notificationFrame.addView(browseText);*/

        pfp = new FrameLayout.LayoutParams((int) elementWitdh, (int) tabHeight);
        //nfp.leftMargin = Utils.getRight(messageFrame);
        pfp.gravity = Gravity.CENTER;
        profileFrame = new FrameLayout(activity);
        profileFrame.setLayoutParams(pfp);
        profileFrame.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams pip = new FrameLayout.LayoutParams(imageSize, imageSize);
        //pip.topMargin = iconTopMargin;
        pip.gravity = Gravity.CENTER;
        profileImage = new ImageView(activity);
        profileImage.setLayoutParams(pip);
        profileFrame.addView(profileImage);


        addTab(newTabSpec("main").setIndicator(homePageFrame),
                HomePageRootFragment.class, null);
        addTab(newTabSpec("search").setIndicator(searchFrame),
                SearchRootFragment.class, null);
        /*addTab(newTabSpec("message").setIndicator(messageFrame),
                MessageRootFragment.class, null);*/
        addTab(newTabSpec("notification").setIndicator(notificationFrame),
                NotificationRootFragment.class, null);
        addTab(newTabSpec("profile").setIndicator(profileFrame),
                ProfileRootFragment.class, null);
        setCurrentTab(0);
    }

    public void onCountryChanged(){
        if(fragmentManager.findFragmentByTag("main")!=null)
            ((HomePageRootFragment)fragmentManager.findFragmentByTag("main")).onCountryChange();
        if(fragmentManager.findFragmentByTag("search")!=null)
            ((SearchRootFragment)fragmentManager.findFragmentByTag("search")).onCountryChange();
    }

    public ActionBar getActionBar() {
        return actionBar;
    }


    public void visibleTabBar(boolean isVisible) {
        if (isVisible)
            tabBar.setVisibility(View.VISIBLE);
        else
            tabBar.setVisibility(View.INVISIBLE);
    }



    @Override
    public void onTabChanged(String tabId) {
        super.onTabChanged(tabId);
    }

    @Override
    public void setCurrentTab(int index) {
        if(getCurrentTab() == index){
            clearStackFragmentByTab();
        }
        super.setCurrentTab(index);
        tabBarRender(index);
    }

    public void clearStackFragmentByTab(){
        if(fragmentManager.findFragmentByTag(Constants.TABS[getCurrentTab()])!=null)
            ((TabFragment)fragmentManager.findFragmentByTag(
                    Constants.TABS[getCurrentTab()])).clearStackFragment();
    }

    public void tabBarRender(int action) {
        setImage();
        currentAction = action;
        if (action == 0) {
            homePageImage.setBackgroundResource(R.drawable.home_active);
            //categoriesText.setTextColor(Color.rgb(69, 90, 100));
        } else if (action == 1) {
            searchImage.setBackgroundResource(R.drawable.search_active);
            //myFavoriesText.setTextColor(Color.rgb(69, 90, 100));
        }/* else if (action == 2) {
            messageImage.setBackgroundResource(R.drawable.message_active);
            //searchText.setTextColor(Color.rgb(69, 90, 100));
        }*/else if (action == 2) {
            notificationImage.setBackgroundResource(R.drawable.notifications_active);
            //browseText.setTextColor(Color.rgb(69, 90, 100));
        }else if (action == 3) {
            profileImage.setBackgroundResource(R.drawable.profile_active);
            //browseText.setTextColor(Color.rgb(69, 90, 100));
        }
    }

    public void setImage() {
        homePageImage.setBackgroundResource(R.drawable.home_passive);
        //categoriesText.setTextColor(Color.rgb(144, 164, 174));

        searchImage.setBackgroundResource(R.drawable.search_passive);
        //myFavoriesText.setTextColor(Color.rgb(144, 164, 174));

        //messageImage.setBackgroundResource(R.drawable.message_passive);
        //searchText.setTextColor(Color.rgb(144, 164, 174));

        notificationImage.setBackgroundResource(R.drawable.notifications_passive);
        //browseText.setTextColor(Color.rgb(144, 164, 174));

        profileImage.setBackgroundResource(R.drawable.profile_passive);
    }
}