package com.alp.sharepoem.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alp.sharepoem.R;
import com.alp.sharepoem.activity.LoginActivity;
import com.alp.sharepoem.activity.MainActivity;
import com.alp.sharepoem.remote.Remotable;
import com.alp.sharepoem.remote.Remote;
import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;
import com.azimolabs.maskformatter.MaskFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SignUpView extends FrameLayout implements Remotable {
    FrameLayout container, passwordFrame , passwordAgainFrame, userNameFrame, numberFrame, nameFrame;
    TextView  loginButton, userNameHeaderText, passwordHeaderText, passwordAgainHeaderText,numberHeaderText, nameHeaderText, loginText;
    EditText userNameEdit, passwordEdit, passwordAgainEdit, numberEdit, nameEdit;
    ImageView topImage, logo;
    Button refreshButton;
    int width, height, sb;
    float tabHeight, actionHeight, u;
    LoginActivity activity;
    Typeface fontRegular, fontBold;
    MaskFormatter maskFormatter;
    Session session;
    int softWareKeysHeight,previousHeightDiffrence;
    boolean isNavBar,isAdd =false;
    JSONArray userAccounts = new JSONArray();
    JSONObject userAccount = new JSONObject();
    JSONArray newUserAccounts = new JSONArray();

    ArrayList<ImageView> lineArray = new ArrayList<>() ;
    int editTextHightSize,textHightSize, textWidthSize, arrowLeftMargin, textHeaderTopMargin, elementFrameSizeHeight,elementFrameLeftMargin, elementFrameSizeWidth, elementFrameTopMargin;


    public SignUpView(Context context) {
        super(context);
        activity = (LoginActivity) context;
        fontRegular = Utils.getRegularFont(activity);
        fontBold = Utils.getBoldFont(activity);
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels - Utils.getStatusBarHeight(getContext());
        sb = Utils.getStatusBarHeight(activity);
        u = Utils.getUnitSize(activity);
        actionHeight = Utils.getActionHeight(activity);
        softWareKeysHeight = Utils.getNavigationBarHeight(activity);
        isNavBar = Utils.hasNavBar();
        render();

    }

    public void render() {

        textHightSize = height * 48 / 1334;
        textWidthSize = width * 260 / 750;
        arrowLeftMargin = width * 260 / 750;
        textHeaderTopMargin = height * 25 / 1334;
        elementFrameSizeHeight = height * 133 / 1334;
        elementFrameLeftMargin = width * 40 / 750;
        elementFrameSizeWidth = width / 2 - width * 60 / 750;
        elementFrameTopMargin = height * 230 / 1334;
        editTextHightSize = height * 90 / 1334;



        LayoutParams cp = new LayoutParams(width, height);
        setLayoutParams(cp);
        container = new FrameLayout(activity);
        container.setLayoutParams(cp);
        container.setFocusableInTouchMode(true);
        container.requestFocus();

        LayoutParams nfp = new LayoutParams(elementFrameSizeWidth * 2, elementFrameSizeHeight);
        nfp.gravity = Gravity.CENTER_HORIZONTAL;
        nfp.topMargin =  elementFrameTopMargin;
        nameFrame = new FrameLayout(activity);
        nameFrame.setLayoutParams(nfp);


        LayoutParams nht = new LayoutParams(textWidthSize * 2, textHightSize);
        nht.gravity = Gravity.CENTER_HORIZONTAL;
        nht.topMargin = textHightSize + textHeaderTopMargin;
        nameHeaderText = new TextView(activity);
        nameHeaderText.setLayoutParams(nht);
        nameHeaderText.setTextColor(Color.BLACK);
        nameHeaderText.setText("Ad - Soyad");
        nameHeaderText.setTypeface(fontBold);
        nameHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        nameHeaderText.setGravity(Gravity.CENTER);
        nameFrame.addView(nameHeaderText);

        LayoutParams nep = new LayoutParams(textWidthSize * 2, editTextHightSize);
        nep.gravity = Gravity.CENTER_HORIZONTAL;
        nep.topMargin =  textHightSize + textHeaderTopMargin;
        nameEdit = new EditText(activity);
        nameEdit.setLayoutParams(nep);
        nameEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        nameEdit.setBackgroundColor(Color.TRANSPARENT);
        nameEdit.setTextColor(Color.BLACK);
        nameEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        nameEdit.setTypeface(fontRegular);
        nameEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        nameEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        nameEdit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                changeParams(0, nameHeaderText, nameEdit);
                return false;
            }
        });
        nameFrame.addView(nameEdit);

        container.addView(nameFrame);
        setLine(nameFrame, elementFrameSizeWidth * 2 + elementFrameLeftMargin * 2);


        LayoutParams unfp = new LayoutParams(elementFrameSizeWidth * 2, elementFrameSizeHeight);
        unfp.gravity = Gravity.CENTER_HORIZONTAL;
        unfp.topMargin = Utils.getBottom(nameFrame) + height * 20 /1334;
        userNameFrame = new FrameLayout(activity);
        userNameFrame.setLayoutParams(unfp);


        LayoutParams unht = new LayoutParams(textWidthSize * 2, textHightSize);
        unht.gravity = Gravity.CENTER_HORIZONTAL;
        unht.topMargin = textHightSize + textHeaderTopMargin;
        userNameHeaderText = new TextView(activity);
        userNameHeaderText.setLayoutParams(unht);
        userNameHeaderText.setTextColor(Color.BLACK);
        userNameHeaderText.setText("Kullanıcı Adı");
        userNameHeaderText.setTypeface(fontBold);
        userNameHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        userNameHeaderText.setGravity(Gravity.CENTER);
        userNameFrame.addView(userNameHeaderText);

        LayoutParams unep = new LayoutParams(textWidthSize * 2, editTextHightSize);
        unep.gravity = Gravity.CENTER_HORIZONTAL;
        unep.topMargin =  textHightSize + textHeaderTopMargin;
        userNameEdit = new EditText(activity);
        userNameEdit.setLayoutParams(unep);
        userNameEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        userNameEdit.setBackgroundColor(Color.TRANSPARENT);
        userNameEdit.setTextColor(Color.BLACK);
        userNameEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        userNameEdit.setTypeface(fontRegular);
        userNameEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        userNameEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        userNameEdit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                changeParams(0, userNameHeaderText, userNameEdit);
                return false;
            }
        });
        userNameFrame.addView(userNameEdit);

        container.addView(userNameFrame);
        setLine(userNameFrame, elementFrameSizeWidth * 2 + elementFrameLeftMargin * 2);

        FrameLayout.LayoutParams nufp = new FrameLayout.LayoutParams(elementFrameSizeWidth * 2, elementFrameSizeHeight);
        nufp.gravity = Gravity.CENTER_HORIZONTAL;
        nufp.topMargin = Utils.getBottom(userNameFrame) + height * 20 /1334;
        numberFrame = new FrameLayout(activity);
        numberFrame.setLayoutParams(nufp);

        FrameLayout.LayoutParams nhtp = new FrameLayout.LayoutParams(textWidthSize * 2, textHightSize);
        nhtp.gravity = Gravity.CENTER_HORIZONTAL;
        nhtp.topMargin = textHightSize + textHeaderTopMargin;
        numberHeaderText = new TextView(activity);
        numberHeaderText.setLayoutParams(nhtp);
        numberHeaderText.setTextColor(Color.BLACK);
        numberHeaderText.setText("Telefon Numarası");
        numberHeaderText.setTypeface(fontBold);
        numberHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        numberHeaderText.setGravity(Gravity.CENTER);
        numberFrame.addView(numberHeaderText);

        FrameLayout.LayoutParams nuep = new FrameLayout.LayoutParams(textWidthSize * 2, editTextHightSize);
        nuep.gravity = Gravity.CENTER_HORIZONTAL;
        nuep.topMargin =  textHightSize + textHeaderTopMargin;
        numberEdit = new EditText(activity);
        maskFormatter = new MaskFormatter("9999 999 99 99", numberEdit);
        numberEdit.setLayoutParams(nuep);
        numberEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        numberEdit.setBackgroundColor(Color.TRANSPARENT);
        numberEdit.setTextColor(Color.BLACK);
        numberEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        numberEdit.setTypeface(fontRegular);
        numberEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
        numberEdit.addTextChangedListener(maskFormatter);
        numberEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        numberEdit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                changeParams(0, numberHeaderText, numberEdit);
                return false;
            }
        });
        numberFrame.addView(numberEdit);

        container.addView(numberFrame);
        setLine(numberFrame, elementFrameSizeWidth * 2 + elementFrameLeftMargin * 2);


        unfp = new LayoutParams(elementFrameSizeWidth * 2, elementFrameSizeHeight);
        unfp.gravity = Gravity.CENTER_HORIZONTAL;
        unfp.topMargin = Utils.getBottom(numberFrame) + height * 20/1334;
        passwordFrame = new FrameLayout(activity);
        passwordFrame.setLayoutParams(unfp);

        unht = new LayoutParams(textWidthSize * 2, textHightSize);
        unht.gravity = Gravity.CENTER_HORIZONTAL;
        unht.topMargin = textHightSize + textHeaderTopMargin;
        passwordHeaderText = new TextView(activity);
        passwordHeaderText.setLayoutParams(unht);
        passwordHeaderText.setTextColor(Color.BLACK);
        passwordHeaderText.setText("Şifre");
        passwordHeaderText.setTypeface(fontBold);
        passwordHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        passwordHeaderText.setGravity(Gravity.CENTER);
        passwordFrame.addView(passwordHeaderText);

        unep = new LayoutParams(textWidthSize * 2, editTextHightSize);
        unep.gravity = Gravity.CENTER_HORIZONTAL;
        unep.topMargin =  textHightSize + textHeaderTopMargin;
        passwordEdit = new EditText(activity);
        passwordEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        passwordEdit.setLayoutParams(unep);
        passwordEdit.setTextColor(Color.BLACK);
        passwordEdit.setBackgroundColor(Color.TRANSPARENT);
        passwordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordEdit.setTypeface(fontRegular);
        passwordEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        passwordEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        passwordEdit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                changeParams(1, passwordHeaderText, passwordEdit);
                return false;
            }
        });
        passwordFrame.addView(passwordEdit);

        container.addView(passwordFrame);
        setLine(passwordFrame, elementFrameSizeWidth * 2 + elementFrameLeftMargin * 2);


        unfp = new LayoutParams(elementFrameSizeWidth * 2, elementFrameSizeHeight);
        unfp.gravity = Gravity.CENTER_HORIZONTAL;
        unfp.topMargin = Utils.getBottom(passwordFrame) + height * 20/1334;
        passwordAgainFrame = new FrameLayout(activity);
        passwordAgainFrame.setLayoutParams(unfp);

        unht = new LayoutParams(textWidthSize * 2, textHightSize);
        unht.gravity = Gravity.CENTER_HORIZONTAL;
        unht.topMargin = textHightSize + textHeaderTopMargin;
        passwordAgainHeaderText = new TextView(activity);
        passwordAgainHeaderText.setLayoutParams(unht);
        passwordAgainHeaderText.setTextColor(Color.BLACK);
        passwordAgainHeaderText.setText("Şifre Tekrarı");
        passwordAgainHeaderText.setTypeface(fontBold);
        passwordAgainHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        passwordAgainHeaderText.setGravity(Gravity.CENTER);
        passwordAgainFrame.addView(passwordAgainHeaderText);

        unep = new LayoutParams(textWidthSize * 2, editTextHightSize);
        unep.gravity = Gravity.CENTER_HORIZONTAL;
        unep.topMargin =  textHightSize + textHeaderTopMargin;
        passwordAgainEdit = new EditText(activity);
        passwordAgainEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        passwordAgainEdit.setLayoutParams(unep);
        passwordAgainEdit.setTextColor(Color.BLACK);
        passwordAgainEdit.setBackgroundColor(Color.TRANSPARENT);
        passwordAgainEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordAgainEdit.setTypeface(fontRegular);
        passwordAgainEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 4 / 8) * 0.60f);
        passwordAgainEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        passwordAgainEdit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                changeParams(1, passwordAgainHeaderText, passwordAgainEdit);
                return false;
            }
        });
        passwordAgainFrame.addView(passwordAgainEdit);

        container.addView(passwordAgainFrame);

        setLine(passwordAgainFrame, elementFrameSizeWidth * 2 + elementFrameLeftMargin * 2);


        LayoutParams lbp = new LayoutParams(elementFrameSizeWidth * 2, height * 120 / 1334);
        lbp.gravity = Gravity.CENTER_HORIZONTAL;
        lbp.topMargin = Utils.getBottom(passwordAgainFrame) + height*20/1334 ;
        loginButton = new TextView(activity);
        loginButton.setLayoutParams(lbp);
        loginButton.setText("Kayıt Ol");
        loginButton.setTypeface(fontRegular);
        loginButton.setBackgroundResource(R.drawable.shadow_button);
        loginButton.setGravity(Gravity.CENTER);
        loginButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Utils.getUnitSize(activity) * 5 / 8) * 0.70f);
        loginButton.setTextColor(Color.WHITE);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isEmpty(userNameEdit.getText().toString())
                        && !Utils.isEmpty(maskFormatter.getRawTextValue().toString())
                        && !Utils.isEmpty(passwordEdit.getText().toString())
                        && !Utils.isEmpty(passwordAgainEdit.getText().toString())) {
                    if (passwordEdit.getText().toString().equals(passwordAgainEdit.getText().toString())){
                        signup();
                    }else {
                        Utils.showDialogMessage(activity, 0, "Uyarı", "Şifreleriniz uyuşmuyor.");
                    }

                } else {
                    /*if (Utils.isEmpty(userNameEdit.getText().toString()))
                        Utils.showDialogMessage(activity, 0, "Uyarı", "Lütfen telefon numaranızı giriniz.");
                    else
                        Utils.showDialogMessage(activity, 0, "Uyarı", "Lütfen şifrenizi giriniz.");*/
                    Utils.showDialogMessage(activity, 0, "Uyarı", "Lütfen bilgilerinizi eksiksiz giriniz.");
                }

            }
        });
        container.addView(loginButton);




        LayoutParams sup = new LayoutParams(width * 250 / 750, textHightSize);
        sup.gravity= Gravity.RIGHT |Gravity.BOTTOM;
        sup.rightMargin = width * 30 / 750;
        sup.bottomMargin = width * 25 / 750;
        loginText = new TextView(activity);
        loginText.setLayoutParams(sup);
        loginText.setText("Giriş Yap!");
        loginText.setTypeface(fontBold);
        loginText.setGravity(Gravity.RIGHT);
        loginText.setTextColor(Color.BLACK);
        loginText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (Utils.isEmpty(maskFormatter.getRawTextValue().toString()))
                    Utils.showDialogMessage(activity, 0, "Meis", "Lütfen telefon numarası giriniz.");
                else {
                    if (!Utils.checkPhone(maskFormatter.getRawTextValue().toString()))
                        Utils.showDialogMessage(activity, 0, "Meis", "Lütfen geçerli bir telefon numarası giriniz.");
                    else
                        getSMS();
                }*/
                LoginView loginView = new LoginView(activity);
                activity.setContentView(loginView);
            }
        });
        container.addView(loginText);

        addView(container);


        if (!isNavBar)
            softWareKeysHeight = 0;
        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                container.getWindowVisibleDisplayFrame(r);
                int keyboardHeight = height - (r.bottom);
                if (previousHeightDiffrence != keyboardHeight) {
                    previousHeightDiffrence = keyboardHeight;
                    if (keyboardHeight > 100) {
                        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
                        params.topMargin = -(height + softWareKeysHeight - r.bottom - sb) / 2;
                        container.setLayoutParams(params);
                    } else {
                        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
                        params.topMargin = 0;
                        container.setLayoutParams(params);
                    }
                }
            }
        });


    }



    public void changeParams(int lineIndex, View headerText, View subText){
        for(ImageView line : lineArray)
            line.setBackgroundColor(Color.rgb(238, 238, 238));
        lineArray.get(lineIndex).setBackgroundColor(Color.rgb(238, 238, 238));

        LayoutParams textHeaderParams = new LayoutParams(textWidthSize, textHightSize);
        textHeaderParams.gravity = Gravity.CENTER_HORIZONTAL;
        textHeaderParams.topMargin = textHeaderTopMargin;
        headerText.setLayoutParams(textHeaderParams);

        if( subText instanceof EditText) {
            LayoutParams textParams = new LayoutParams(textWidthSize * 2, editTextHightSize);
            textParams.gravity = Gravity.CENTER_HORIZONTAL;
            textParams.topMargin = Utils.getBottom(headerText) - (height * 15 / 1334);
            subText.setLayoutParams(textParams);
        }
    }

    public void setLine(FrameLayout frame, int lineWidth) {
        ImageView line = new ImageView(activity);
        LayoutParams vp = new LayoutParams(lineWidth, height * 3 / 1334);
        vp.gravity = Gravity.BOTTOM  | Gravity.CENTER_HORIZONTAL;
        line.setLayoutParams(vp);
        line.setBackgroundColor(Color.rgb(238, 238, 238));
        frame.addView(line);
        lineArray.add(line);
    }


    public void signup() {
        activity.showLoadingActivity();
        Remote remote = new Remote(activity, this);
        JSONObject params = new JSONObject();
        try {
            params.put("userName", userNameEdit.getText().toString());
            params.put("phone", maskFormatter.getRawTextValue().toString());
            params.put("password", passwordEdit.getText().toString());
            params.put("name",nameEdit.getText().toString());
            //params.put("device", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        remote.callJsonAsync("POST", Constants.URL + Constants.URL_SIGN_UP, params, "signup", false);
    }

    @Override
    public void responsed(String method, JSONObject response) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.dismisLoadingActivity();
            }
        });
        if (method.equals("login")) {
            session = Session.getSession(activity);
            Intent intent = new Intent(activity, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
        }else if(method.equals("sendPassword")){
            try {
                Utils.showDialogMessage(activity,0,"",response.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (method.equals("signup")){
            Utils.showDialogMessage(activity,0,"","Kayıt Başarılı");
        }
    }

    @Override
    public void failed(String method, JSONObject response) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.dismisLoadingActivity();
            }
        });
        try {
            Utils.showDialogMessage(activity,0,"",response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}