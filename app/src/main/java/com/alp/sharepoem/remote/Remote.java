package com.alp.sharepoem.remote;

import android.content.Context;
import android.os.AsyncTask;


import com.alp.sharepoem.session.Session;
import com.alp.sharepoem.utils.Constants;
import com.alp.sharepoem.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Remote {

    Context context;
    Remotable remotable = null;
    Session session;
    public static CookieStore cookieStore = null;

    public Remote(Context context, Remotable remotable) {
        this.context = context;
        this.remotable = remotable;
        if (Remote.cookieStore == null) {
            Remote.cookieStore = new BasicCookieStore();
        }
    }

    public void callAsync(Object... params) {
        new RemoteTask().execute(params);
    }

    public void callUploadAsync(Object... params) {
        new UploadTask().execute(params);
    }

    public void callJsonAsync(Object... params) {
        new JsonTask().execute(params);
    }


    class RemoteTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object... params) {

            final String requestType = (String) params[0];
            final String url = (String) params[1];
            JSONObject jParams = (JSONObject) params[2];
            final String method = (String) params[3];
            boolean isArray = (boolean) params[4];
            session = Session.getSession(context);

            HttpContext localContext = new BasicHttpContext();
            localContext.setAttribute(ClientContext.COOKIE_STORE, Remote.cookieStore);

            HttpRequestBase request = null;

            if (requestType.equals("POST"))
                request = requestPost(url, jParams);
            else
                request = requestGet(url, jParams);

            /*if (Utils.isSetToken(url))
                request.setHeader("Authorization", "Bearer " + session.getString("token"));*/

            HttpClient client = new DefaultHttpClient();
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                HttpResponse hresponse = client.execute(request, localContext);
                hresponse.getEntity().writeTo(out);
                out.close();
                String response = out.toString("UTF-8");
                JSONObject json = new JSONObject();
                if (!Utils.isEmpty(response)) {
                    if (isArray)
                        json.put("array", new JSONArray(response));
                    else
                        json = new JSONObject(response);
                }
                if (hresponse.getStatusLine().getStatusCode() == 200)
                    remotable.responsed(method, json);
                else
                    remotable.failed(method, json);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        public HttpPost requestPost(String url, JSONObject jParams) {
            HttpPost request;
            request = new HttpPost(url);
            MultipartEntity entity = new MultipartEntity();
            try {
                Iterator<?> keys = jParams.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    entity.addPart(key, new StringBody(jParams.getString(key), Charset.forName("UTF-8")));
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            request.setEntity(entity);
            return request;
        }


        public HttpGet requestGet(String url, JSONObject jParams) {

            HttpGet request = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            try {
                Iterator<?> keys = jParams.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    nameValuePairs.add(new BasicNameValuePair(key, jParams.getString(key)));
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            String paramsString = URLEncodedUtils.format(nameValuePairs, "UTF-8");
            request = new HttpGet(url + "?" + paramsString);
            return request;
        }

    }

    class JsonTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object... params) {

            final String requestType = (String) params[0];
            final String url = (String) params[1];
            JSONObject jParams = (JSONObject) params[2];
            final String method = (String) params[3];
            boolean isArray = (boolean) params[4];
            session = Session.getSession(context);

            HttpContext localContext = new BasicHttpContext();
            localContext.setAttribute(ClientContext.COOKIE_STORE, Remote.cookieStore);

            HttpPost request = new HttpPost(url);
            String message = jParams.toString();
            try {
                request.setEntity(new StringEntity(message, "UTF8"));
                request.setHeader("Content-type", "application/json");
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*if (Utils.isSetToken(url))
                request.setHeader("Authorization", "Bearer " + session.getString("token"));*/

            HttpClient client = new DefaultHttpClient();
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                HttpResponse hresponse = client.execute(request, localContext);
                hresponse.getEntity().writeTo(out);
                out.close();
                String response = out.toString("UTF-8");
                JSONObject json = new JSONObject();
                if (isArray)
                    json.put("array", new JSONArray(response));
                else
                    json = new JSONObject(response);

                if (hresponse.getStatusLine().getStatusCode() == 200)
                    remotable.responsed(method, json);
                else
                    remotable.failed(method, json);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    class UploadTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object... params) {
            final String method = (String) params[0];
            JSONObject jParams = (JSONObject) params[1];
            MultipartEntity entity = new MultipartEntity();
            try {
                Iterator<?> keys = jParams.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if (key.equals("image")) {
                        entity.addPart(key, new FileBody((File) jParams.get(key)));
                        continue;
                    }
                    entity.addPart(key, new StringBody(jParams.getString(key)));
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            HttpPost post;
            post = new HttpPost(Constants.UPLOAD_HOST_PROD + "" + method);

            post.setEntity(entity);
            HttpClient client = new DefaultHttpClient();
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                HttpResponse hresponse = client.execute(post);
                hresponse.getEntity().writeTo(out);
                out.close();
                String response = out.toString("UTF-8");
                final JSONObject json = new JSONObject(response);
                remotable.responsed(method, json);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

    }
}
