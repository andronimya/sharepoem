package com.alp.sharepoem.remote;

import org.json.JSONObject;


public interface Remotable {
        void responsed(String method, JSONObject response);
        void failed(String method, JSONObject response);
}
